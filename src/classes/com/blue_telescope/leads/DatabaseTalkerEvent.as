package com.blue_telescope.leads {
	import flash.events.Event;

	/**
	 * @author lera
	 */
	public class DatabaseTalkerEvent extends Event {
		public static var DATA_READY : String = "ServerEvent:: data ready";
		public static var IMG_SAVE_SUCCESS : String = "ServerEvent:: image is saved";
		public static var IMG_SAVE_FAIL: String = "ServerEvent:: image save failed";
		public var data : *;
		public function DatabaseTalkerEvent(type : String, data : * = null, bubbles : Boolean = false, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);
			this.data = data;
			
		}
        public override function clone():Event {
            return new DatabaseTalkerEvent(type, data);
        }		
	}
}
