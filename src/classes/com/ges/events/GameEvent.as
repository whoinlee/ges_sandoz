package com.ges.events {
	import flash.events.Event;

	/**
	 * Game Events
	 */
	public class GameEvent extends Event {
		
		public static const START						:String = "start";
		public static const RESTART						:String = "restart";
		public static const ISI							:String = "isi";
		public static const REF							:String = "ref";
		public static const QUIT						:String = "quit";
		public static const TIMEOUT						:String = "timeout";
		public static const UPDATE_SCORE				:String = "updateScore";
		
		public var scoreOffset							:int = 0;
		public var refID								:uint = 11;
		
		
		public function GameEvent(type:String, pScoreOffset:uint = 0, pRefID:uint = 11, bubbles:Boolean = false, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
			this.scoreOffset = pScoreOffset;
			this.refID = pRefID;
		}
		
        public override function clone():Event {
            return new GameEvent(type, scoreOffset, refID);
        }
	}//c
}//p