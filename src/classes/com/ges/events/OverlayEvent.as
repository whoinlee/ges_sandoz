package com.ges.events {
	import flash.events.Event;

	/**
	 * Overlay Events
	 */
	public class OverlayEvent extends Event {
		
		public static const CLOSE						:String = "close";
		public static const CONTINUE					:String = "continue";
		public static const QUIT						:String = "quit";
		
		
		public function OverlayEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
		}
	}
}
