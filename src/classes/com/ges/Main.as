package com.ges {
	import com.blue_telescope.leads.CSharpTalker;
	import com.blue_telescope.leads.CSharpTalkerCustomEvent;
	import com.blue_telescope.leads.Person;
	import com.ges.assets.ContentBkg;
	import com.ges.data.*;
	import com.ges.manager.*;
	import com.ges.view.*;
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	import com.greensock.easing.Quad;
	import com.greensock.plugins.ColorTransformPlugin;
	import com.greensock.plugins.TintPlugin;
	import com.greensock.plugins.TweenPlugin;

	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.system.fscommand;
	import flash.ui.Mouse;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[SWF(backgroundColor="#ffffff", frameRate="30", width="1920", height="1080")]
	public class Main extends Sprite {

		//-- views	
		private var _scorePanel1:ScorePanel1View;
		private var _scorePanel2:ScorePanel2View;
		private var _welcomeView:WelcomeView;
		private var _categoryView:CategoryView;
		private var _overlayPanel:OverlayPanelView;
		private var _gameView:GameView;
		private var _resultView:ResultView;
		
		private var _viewHolder:Sprite;
		private var _currentView:BaseView;

		private var _bkgHolder:Sprite;
		private var _bkg:ContentBkg;
		
		private var _attractLoop:Sprite;
		private var _video:Video;
		private var _ns:NetStream;
		
		//-- singletons
		private var _dataModel:DataModel;
		private var _controller:Controller;
		private var _styleManager:StyleManager;
		private var _soundController:SoundController;
		
		//-- intervalIDs
		private var _onIdleTimeOutID:Number;
		private var _hideScorePanel1ID:Number;
		private var _showScorePanel1ID:Number;
		
		private var _destroyOverlayID:Number;
		private var _destroyGameID:Number;
		private var _showCategoryID:Number;
		
		
		public function Main() 
		{
			TweenPlugin.activate([TintPlugin, ColorTransformPlugin]);
			
			_dataModel = DataModel.getInstance();
			_dataModel.getData(Config.CONFIG_PATH);
			//
			_controller = Controller.getInstance();
			_controller.registerApp(this);
			//
			_styleManager = StyleManager.getInstance();
			_styleManager.loadStyle(Config.STYLE_PATH);
			//
			_soundController = SoundController.getInstance();
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.SHOW_ALL;
			stage.showDefaultContextMenu = false;
			
			if (_dataModel.isDataReady) {
				init();
			} else {
				_dataModel.addEventListener(DataModel.DATA_READY, init);
			}
		}
		
		private function init(e:Event = null) : void 
		{
			if (Config.hideCursor) Mouse.hide();
			if (Config.fullScreen) fscommand("fullscreen","true");
			
			_bkgHolder = new Sprite();
			addChild(_bkgHolder);
			buildBkg();
			hideBkg();
			
			_viewHolder = new Sprite();
			addChild(_viewHolder);
			_viewHolder.visible = false;
		
			//-- commented out by WhoIN. initTouch() updated instead
//			if (!Config.isSwipe) {
//				CSharpTalker.dispatcher.addEventListener(CSharpTalkerCustomEvent.GOT_DATA, onCSharpTalkerData);	//-- Valeria
//			}

			initTouch();
			showAttractLoop();
			buildScorePanel1();
		}

		private function buildAttractLoop():void
		{
			destroyAttractLoop();
			
			_attractLoop = new Sprite();
			addChild(_attractLoop);
			
			_video = new Video();
			_video.width = Config.STAGE_W;
			_video.height = Config.STAGE_H;
			_attractLoop.addChild(_video);
			
			var nc:NetConnection = new NetConnection();
			nc.connect(null);

			_ns = new NetStream(nc);
			_video.attachNetStream(_ns);
			var metaSniffer:Object = new Object();
			_ns.client = metaSniffer;
			metaSniffer.onPlayStatus = getPlayStatus;
			
			_ns.play(Config.attractPath);
		}
		
		private function destroyAttractLoop():void
		{
			if (_attractLoop) {
				if (_ns) {
					_ns.pause();
					_ns.close();
				}
				if (_video) _video.clear();	
				_attractLoop.removeChild(_video);
				_video = null;
				_ns = null;
				
				removeChild(_attractLoop);
				_attractLoop = null;
			}
		}
		
		public function showAttractLoop(pDelay:Number = 0):void
		{
//			trace("INFO Main :: showAttractLoop");
			
			
			//-- by Valeria
			if (_dataModel.currentUser) {
				onGameEnd();
			}
			
			//-- commented out by WhoIN. initTouch() updated instead, for Config.isSwipe & for !Config.isSwipe
//			if (Config.isSwipe) {
//				CSharpTalker.dispatcher.addEventListener(CSharpTalkerCustomEvent.GOT_DATA, onCSharpTalkerData);
//			}
			//-----------------
			
			_dataModel.reset();	//added on 05/24
			
			if (_attractLoop == null) {
				buildAttractLoop();
			} else {
				_ns.seek(0);
				_ns.resume();
			}
			
			_attractLoop.alpha = 0;
			_attractLoop.visible = true;
			TweenLite.to(_attractLoop, .75, {delay:pDelay, alpha:1, ease:Quad.easeOut});
		}
		
		public function hideAttractLoop():void
		{
			if (_attractLoop) {
				_ns.seek(0);
				_ns.pause();
				_attractLoop.visible = false;
			}
		}
		
		private function initTouch():void
		{
//			trace("INFO Main :: initTouch");
			
			if (!Config.isSwipe) {
				stage.addEventListener(MouseEvent.MOUSE_DOWN, onTouchScreen);
			} else {
				//Valeria, added by WhoIN
				CSharpTalker.dispatcher.addEventListener(CSharpTalkerCustomEvent.GOT_DATA, onCSharpTalkerData);
			}
		}
		
		private function killTouch():void
		{
//			trace("INFO Main :: killTouch");
			
			if (!Config.isSwipe) {
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, onTouchScreen);
			} else {
				//Valeria, added by WhoIN
				CSharpTalker.dispatcher.removeEventListener(CSharpTalkerCustomEvent.GOT_DATA, onCSharpTalkerData);
			}
		}
		
		private function onGameEnd() : void 
		{
			//-- by Valeria, updated by WhoIN
			updateUserData();
			saveUserData();				 																							
		}

		private function updateUserData():void
		{
			Person.firstName = _dataModel.currentUser.name;		
			Person.score = _dataModel.currentUser.score;
			Person.ranking = _dataModel.currentUser.ranking;
			Person.acro = _dataModel.currentUser.acro;
			Person.updateXML();
			
//			trace("\nINFO Main :: updateUserData\n" + Person.personXML + "\n");	
		}
		
		private function saveUserData():void
		{
//			CSharpTalker.sendUserData(Person.personXML);	//calling order changed after addQuizData
			
			var selectedAnswerArr:Array = _dataModel.selectedAnswerArr;
			var total:uint = selectedAnswerArr.length;
			for (var i:uint = 0; i<total; i++) {
				var answerID:String = selectedAnswerArr[i];
				var quizID:String = answerID.substr(0, 5);
				CSharpTalker.addQuizData(quizID, answerID, 0, false);
			}
			
			//-- calling order changed (5/24)
			trace(Person.personXML);
			if (Config.isSwipe) CSharpTalker.sendUserData(Person.personXML);
		}
		
		private function backToInit(e:MouseEvent = null):void
		{
//			trace("INFO Main :: backToInit");
			
			_currentView = null;
			destroyOverlay();
			hideBkg();
			
			initTouch();
			showAttractLoop();
			buildScorePanel1();
		}
		
		private function resetIdleTimer(e:MouseEvent = null):void
		{
			if (Config.idleTimeout > 0) {
				clearTimeout(_onIdleTimeOutID);
				_onIdleTimeOutID = setTimeout(quitGame, Config.idleTimeout);	//on idleTimeout from welcomeView
			}
		}

		private function buildBkg():void
		{
			_bkg = new ContentBkg();
			_bkgHolder.addChild(_bkg);
		}
		
		private function hideBkg():void
		{
			_bkgHolder.visible = false;
		}
		
		private function showBkg():void
		{
			_bkgHolder.visible = true;
		}
		
		private function buildWelcome():void
		{
			trace("INFO Main :: buildWelcome");
				
			destroyCurrent();
			
			_welcomeView = new WelcomeView();
			_viewHolder.addChild(_welcomeView);
			
			_currentView = _welcomeView;
			_viewHolder.visible = true;
			
			resetIdleTimer();
			stage.addEventListener(MouseEvent.MOUSE_MOVE, resetIdleTimer);
			
			_welcomeView.alpha = 0;
			_welcomeView.visible = true;
			_welcomeView.activateStart();
			TweenLite.to(_welcomeView, .75, {alpha:1, ease:Quad.easeOut});
			
//			_dataModel.reset();	//commented out, 5/24
		}
		
		private function destroyWelcome():void
		{
//			trace("INFO Main :: destroyWelcome");
			
			clearTimeout(_onIdleTimeOutID);
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, resetIdleTimer);

			if (_welcomeView) {
				_welcomeView.visible = false;
				_welcomeView.destroy();
				_viewHolder.removeChild(_welcomeView);
				_welcomeView = null;
			}
		}
		
		private function buildScorePanel1():void
		{
			_scorePanel1 = new ScorePanel1View();
			_scorePanel1.y = Config.STAGE_H + ScorePanel1View.BKG_H;
			addChild(_scorePanel1);
			
			showScorePanel1();
		}
		
		private function showScorePanel1():void
		{
			var targetY:Number = Config.STAGE_H - ScorePanel1View.BKG_H;
			TweenLite.to(_scorePanel1, .75, {y:targetY, ease:Back.easeOut});
			_hideScorePanel1ID = setTimeout(hideScorePanel1, 8*1000);	//for 8 sec
		}
		
		private function hideScorePanel1():void
		{
			var targetY:Number = Config.STAGE_H + ScorePanel1View.BKG_H;
			TweenLite.to(_scorePanel1, 1, {y:targetY, ease:Quad.easeOut});
			_showScorePanel1ID = setTimeout(showScorePanel1, 8*1000);
		}
		
		private function destroyScorePanel1():void
		{
			clearTimeout(_showScorePanel1ID);
			clearTimeout(_hideScorePanel1ID);
			
			if (_scorePanel1) {
				_scorePanel1.destroy();
				removeChild(_scorePanel1);
				_scorePanel1 = null;
			}
		}
		
		private function buildCategory():void
		{
//			trace("INFO Main :: buildCategory");
			
			_categoryView = new CategoryView();
			_viewHolder.addChild(_categoryView);
			
			_currentView = _categoryView;
			_viewHolder.visible = true;
			
			_categoryView.alpha = 0;
			_categoryView.visible = true;
			TweenLite.to(_categoryView, .75, {alpha:1, ease:Quad.easeOut});
		}
		
		private function showCategory():void
		{
			trace("INFO Main :: showCategory");

			clearTimeout(_showCategoryID);
			
			destroyOverlay();
			destroyCurrent(_categoryView);
			
			_categoryView.alpha = 0;
			_categoryView.visible = true;
			TweenLite.to(_categoryView, .5, {alpha:1, ease:Quad.easeOut});
			
			_currentView = _categoryView;
			_viewHolder.visible = true;
			
			_scorePanel2.resetOffset();
			_scorePanel2.activateButtons();
			_scorePanel2.deactivateRefBt();
			_scorePanel2.hideRefBt();
			
			if (_categoryView.selectedButtonCount == CategoryView.BUTTON_TOTAL) {
				TweenLite.delayedCall(2, quitGame);
			}
		}
				
		private function hideCategory():void
		{
//			trace("INFO Main :: hideCategory");
			_categoryView.visible = false;
		}
		
		private function destroyCategory():void
		{
//			trace("INFO Main :: destroyCategory");
			if (_categoryView) {
				_categoryView.destroy();
				_viewHolder.removeChild(_categoryView);
				_categoryView = null;
			}
		}
				
		private function buildScorePanel2():void
		{
			_scorePanel2 = new ScorePanel2View();
			_scorePanel2.y = Config.STAGE_H;
			addChild(_scorePanel2);
			_scorePanel2.visible = false;
		}
		
		private function destroyScorePanel2():void
		{
//			trace("INFO Main :: destroyScorePanel2");
			if (_scorePanel2) {
				_scorePanel2.destroy();
				removeChild(_scorePanel2);
				_scorePanel2 = null;
			}
		}
		
		private function showScorePanel2():void
		{
//			trace("INFO Main :: showScorePanel2");
			_scorePanel2.visible = true;
			var targetY:Number = 972;
			TweenLite.to(_scorePanel2, .5, {delay:.1, y:targetY, ease:Back.easeOut});
		}
		
		private function buildGame(pQuizData:QuizData):void
		{
//			trace("INFO Main :: buildGame");
			
			_gameView = new GameView(pQuizData);
			_viewHolder.addChild(_gameView);
			
			_currentView = _gameView;
			_viewHolder.visible = true;
			
			_gameView.alpha = 0;
			_gameView.visible = true;
			TweenLite.to(_gameView, .75, {alpha:1, ease:Quad.easeOut});

		}
		
		private function destroyGame():void
		{
			clearTimeout(_destroyGameID);
			
			if (_gameView) {
				_gameView.destroy();
				_viewHolder.removeChild(_gameView);
				_gameView = null;
			}
		}
		
		private function buildResult():void
		{
//			trace("INFO Main :: buildResult");

			destroyCurrent();
			
			var pageID:uint = 1;
			if (_dataModel.isTop10()) pageID = 2;
			
			_resultView = new ResultView(pageID);
			_viewHolder.addChild(_resultView);
			
			_currentView = _resultView;
			_viewHolder.visible = true;
			
			_resultView.alpha = 0;
			_resultView.visible = true;
			TweenLite.to(_resultView, .75, {alpha:1, ease:Quad.easeOut});
		}
		
		private function destroyResult():void
		{
//			trace("INFO Main :: destroyResult, _resultView is " + _resultView);
			if (_resultView) {
				_resultView.destroy();
				_viewHolder.removeChild(_resultView);
				_resultView = null;
			}
		}
		
		private function destroyCurrent(except:BaseView = null):void
		{
			if (except != null) {
				trace("INFO destroyCurrent, except.viewName is " + except.viewName);
			}
			
			if (_currentView != null) {
				trace("INFO destroyCurrent, _currentView.viewName is " + _currentView.viewName);
				
				if (_welcomeView && (_currentView == _welcomeView)) {
					if (except != _welcomeView)
					destroyWelcome();
				}
				if (_categoryView && (_currentView == _categoryView)) {
					if (except != _categoryView)
					destroyCategory();
				}
				if (_gameView && (_currentView == _gameView)) {
					if (except != _gameView)
					destroyGame();
				}
				if (_resultView && (_currentView == _resultView)) {
					if (except != _resultView)
					destroyResult();
				}
			}
		}
		

		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public function buildOverlay(pKind:String = "isi"):void
		{
			trace("INFO Main :: buildOverlay, overlay kind is " + pKind);
			
			destroyOverlay();
			
			if (pKind == "ref" && _gameView) {
				var refTypeID:uint = _gameView.refID;
				_overlayPanel = new OverlayPanelView(pKind, refTypeID);
			} else {
				_overlayPanel = new OverlayPanelView(pKind);
			}
			addChild(_overlayPanel);
			
			_overlayPanel.show();
			
			//-- 5/20
			if (_scorePanel2) _scorePanel2.stopTimer();
			if (_gameView) _gameView.stopTimer();
		}
		
		public function destroyOverlay():void
		{
			if (_destroyOverlayID) clearTimeout(_destroyOverlayID);
			
			if (_overlayPanel) {
				_overlayPanel.destroy();
				removeChild(_overlayPanel);
				_overlayPanel = null;
				
				//-- 5/20
				if (_scorePanel2) _scorePanel2.restartTimer();
				if (_gameView) _gameView.restartTimer();
			}
		}
		
		public function startGame():void
		{
			//-- added by WhoIN, incorporating Valeria's update
			updateUserData();
			
			//-- start game from the welcome view
			destroyWelcome();
			buildCategory();
			buildScorePanel2();
			showScorePanel2();
		}
		
		public function restartGame():void
		{
			trace("INFO Main :: restartGame");
			
			//-- added by WhoIN, incorporating Valeria's update
			updateUserData();
			
			//-- create a new user data with the current user name
			var currentUserName:String = _dataModel.currentUser.name;
			_dataModel.currentUser = new UserData(currentUserName);
			
			//-- back to the welcome view
			destroyResult();
			buildWelcome();
		}
		
		public function showQuiz(selectedCategoryIndex:uint, selectedQuizIndex:uint):void
		{
//			trace("INFO Main :: showQuiz");
			
			hideCategory();
			
			var selectedCategoryData:CategoryData = _dataModel.categoryDataArr[selectedCategoryIndex];
			var selectedQuizData:QuizData = selectedCategoryData.quizDataArr[selectedQuizIndex]; 
			if (_gameView) {
				trace("INFO Main :: showQuiz, reset gameView data");
				_gameView.data = selectedQuizData;
			} else {
				trace("INFO Main :: showQuiz, build gameView");
				buildGame(selectedQuizData);
			}
			_gameView.visible = true;
			_currentView = _gameView;
			
			if (_scorePanel2) {
				_scorePanel2.activateButtons();
				_scorePanel2.showRefBt();
			}
		}
		
		public function hideGame():void
		{
			if (_gameView) {
				_gameView.visible = false
			}
			_currentView = null;
		}
		
		public function backToCategory(pDelay:uint = 0):void
		{
			trace("INFO Main :: backToCategory");
			
			clearTimeout(_destroyOverlayID);
			clearTimeout(_destroyGameID);
			clearTimeout(_showCategoryID);
			_destroyOverlayID = setTimeout(destroyOverlay, pDelay*1000);//CHECK
			_destroyGameID = setTimeout(destroyGame, pDelay*1000);
			_showCategoryID = setTimeout(showCategory, pDelay*1000);

//			destroyOverlay();
//			destroyGame();
//			showCategory();
		}
		
		public function quitGame():void
		{
//			trace("INFO Main :: quitGame, _currentView.viewName is " + _currentView.viewName);

			clearTimeout(_destroyOverlayID);
			clearTimeout(_destroyGameID);
			clearTimeout(_showCategoryID);
			
			destroyOverlay();
			if (_scorePanel2) _scorePanel2.onQuit();
			if (_gameView) _gameView.killTimer();

			var viewName:String = _currentView.viewName;
			switch (viewName) {
				case "Welcome":
					destroyWelcome();
					backToInit();
					break;
				case "Category":
					destroyCategory();
					destroyScorePanel2();
					buildResult();
					break;
				case "Game":
					destroyGame();
					destroyScorePanel2();
					buildResult();
					break;
				case "Result":
					destroyResult();
					backToInit();
					break;
				default:
					destroyWelcome();
					destroyCategory();
					destroyScorePanel2();
					destroyResult();
					destroyGame();
					backToInit();
			}
		}
		
		public function onTimeOut():void
		{
			if (_currentView.viewName == "Game") _gameView.killTimer();
			quitGame();
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function getPlayStatus(pstatus:Object):void
		{ 
//			trace("INFO Main :: getPlayStatus, pstatus.code is " + pstatus.code);
			if (pstatus.code == "NetStream.Play.Complete") {
				trace("INFO Main :: getPlayStatus, NetStream.Play.Complete");
				_ns.seek(0);
			}
		}
		
		private function onTouchScreen(e:MouseEvent):void
		{
			trace("INFO Main :: onTouchScreen - Config.isSwipe==false");
			
			//-- Valeria
			Person.build();
			Person.firstName = "Bluetelescope Tester";
			//-----------
			
			killTouch();
			hideAttractLoop();
			destroyScorePanel1();
			
			//-- for testing
			_dataModel.currentUser = new UserData(Person.firstName);
			
			showBkg();
			buildWelcome();
		}
						
		private function onCSharpTalkerData(event:CSharpTalkerCustomEvent):void 
		{ 
			trace("INFO Main :: onCSharpTalkerData, by Valeria - Config.isSwipe==true");
			
			//-- by Valeria
			CSharpTalker.dispatcher.removeEventListener(CSharpTalkerCustomEvent.GOT_DATA, onCSharpTalkerData);		 //---> moved to killTouch					
			Person.build(event.params.msg);	
			
			killTouch();
			hideAttractLoop();
			destroyScorePanel1();
			
			_dataModel.currentUser = new UserData(Person.firstName);
					
			showBkg();
			buildWelcome();
			//-----------				
        }
	}//c
}//p