package com.ges.ui {
	import com.ges.assets.ISIPaneContent;

	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	// import com.greensock.TweenLite;
//	import flash.utils.getDefinitionByName;
	
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class ISIPane extends Sprite {
		
		private static const MASK_W				:Number = 1260;
		private static const MASK_H				:Number = 688;
		private static const CONTENT_X			:Number = -650;
		private static const CONTENT_Y			:Number = -330;
		
		private var _contentHolder				:Sprite;
		private var _mask						:Shape;
		private var _scrollBar					:ScrollBar;
		private var _isiContent					:ISIPaneContent;
		
		private var _minY						:Number;
		
		private var _mouseDown					:Boolean = false;
		
		
		public function ISIPane() {
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			configUI();
		}
		
		private function configUI() : void
		{
			_contentHolder = new Sprite();
			_contentHolder.x = CONTENT_X;
			_contentHolder.y = CONTENT_Y;
			addChild(_contentHolder);
			
			_contentHolder.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
			_contentHolder.addEventListener(MouseEvent.MOUSE_OUT, onMouseUpHandler);
			_contentHolder.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
			
			_isiContent = new ISIPaneContent();
			_contentHolder.addChild(_isiContent);
			
			_mask = new Shape();
			_mask.x = CONTENT_X;
			_mask.y = CONTENT_Y;
			var mg:Graphics = _mask.graphics;
			mg.beginFill(0x33cc33, .5);
			mg.drawRect(0, 0, MASK_W, MASK_H);
			mg.endFill();
			addChild(_mask);
			
			_contentHolder.mask = _mask;
			
			_scrollBar = new ScrollBar();
			_scrollBar.x = 623;
			_scrollBar.y = -330;
			addChild(_scrollBar);
			_scrollBar.addEventListener("scroll", onScroll);
			_scrollBar.visibleRatio = MASK_H/_contentHolder.height;
			
			_minY = CONTENT_Y -(_contentHolder.height) + MASK_H;
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onScroll(e:Event):void
		{
//			trace("onScroll, _scrollBar.value is " + _scrollBar.value);
			
			var diff:Number = _contentHolder.height - MASK_H;
			var targetY:Number = Math.floor(CONTENT_Y - (diff*_scrollBar.value/100));
			
			if (targetY < _minY) {
				targetY = _minY;
			} else if (targetY > CONTENT_Y) {
				targetY = CONTENT_Y;
			}
			_contentHolder.y = targetY;
		}
		
		private function onMouseDownHandler(event:MouseEvent):void 
		{
//			trace("INFO ISIPane :: onMouseDownHandler");
			
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			_mouseDown = true;
			_contentHolder.startDrag(false, new Rectangle(CONTENT_X, CONTENT_Y, 0, _minY));
		}
		
		private function onMouseUpHandler(event:MouseEvent):void 
		{
//			trace("INFO ISIPane :: onMouseUpHandler");
			
			//trace("onMouseMoveHandler value is " + value);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			_mouseDown = false;
			_contentHolder.stopDrag();
		}

		private function onMouseMoveHandler(event:MouseEvent):void 
		{
//			trace("INFO ISIPane :: onMouseMoveHandler");
			
			if (_mouseDown) {
				//trace("INFO ScrollBar :: value is " + value);
			
			var diff:Number = _contentHolder.height - MASK_H;
			var value:Number = - Math.floor((100 * (_contentHolder.y - CONTENT_Y))/diff);
			
			if (value <0) value = 0;
			if (value > 100) value = 100;
			
			_scrollBar.value = value;
			}
		}
	}//c
}//p