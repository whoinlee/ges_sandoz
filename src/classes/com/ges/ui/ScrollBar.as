package com.ges.ui {
	import com.ges.assets.ScrollBarAsset;

	import flash.display.*;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	import com.greensock.TweenLite;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="scroll", type="flash.events.Event")]
	public class ScrollBar extends Sprite {

		public static const SCROLL			:String = "scroll";

		private static const MIN_RATIO		:Number = 0.044;
		private static const TRACK_H		:Number = 688;
		private static const SCROLLER_MIN_H	:Number = 30;
					
		private var _asset:ScrollBarAsset;
		
		private var _track:MovieClip;
		private var _scroller:MovieClip;
		
		private var _minimum:Number = 0;
		private var _maximum:Number = 100;
		private var _mouseDown:Boolean = false;
		
		private var _maxH:Number = TRACK_H - SCROLLER_MIN_H;
		private var _maxY:Number = TRACK_H - SCROLLER_MIN_H;	
		
//		private var _trackH:Number = TRACK_H;
		private var _scrollerH:Number = SCROLLER_MIN_H;
		
		private var _visibleRatio:Number = MIN_RATIO;	//30/688
		public function set visibleRatio(r:Number):void {
//			trace("INFO ScrollBar:: visibleRatio is " + r);
			_visibleRatio = (r<MIN_RATIO)? MIN_RATIO : r;
			updateScroller();
		}
		
		private var __value:Number = 0;	//0 to 100 scale
		public function get value():Number {
			__value =  (_scroller.y)*(_maximum - _minimum)/(_maxH);
			if (__value > 100) __value = 100;
			return __value;
		}
		
		public function set value(v:Number):void
		{
			__value = v;
			_scroller.y = (__value * _maxH)/(_maximum - _minimum);
		}
		
		public function set enabled(b:Boolean):void {
			this.visible = b;
		}
		
		
		
		////////////////////////////////////////////////////////////////////////////
		// Constructor
		////////////////////////////////////////////////////////////////////////////
		public function ScrollBar() 
		{
			addEventListener(Event.ADDED_TO_STAGE, configUI);
		}
		
		private function configUI(e:Event):void
		{
			_asset = new ScrollBarAsset();
			addChild(_asset);
			
			_track = _asset.track;
			_track.addEventListener(MouseEvent.CLICK, onGutterClickHandler);
			
			_scroller = _asset.scroller;
			_scroller.buttonMode = _scroller.mouseEnabled = true;
			_scroller.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
//			_scroller.addEventListener(MouseEvent.MOUSE_OUT, onMouseUpHandler);
			_scroller.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);

			reset();
			updateScroller();
		}
		
		private function updateScroller():void
		{
//			trace("INFO ScrollBar:: updateScroller, _scroller is " + _scroller);
			
			_scrollerH = Math.floor(_visibleRatio * TRACK_H);
			if (_scrollerH < SCROLLER_MIN_H) _scrollerH = SCROLLER_MIN_H;
			
			_scroller.middle.height = _scrollerH - 20;
			_scroller.middle.y = 10;
			_scroller.bottom.y = _scroller.middle.y + _scroller.middle.height;
			
			_maxY = _maxH = TRACK_H - _scrollerH;
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public function setScrollProperties(minimum:Number, maximum:Number):void 
		{
			_minimum = minimum;
			_maximum = maximum;
		}
		
		public function reset():void
		{
			__value = 0;
			_scroller.y = 0;
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onMouseDownHandler(event:MouseEvent):void {
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			
			TweenLite.killTweensOf(_scroller);
			TweenLite.to(_scroller, .1, {colorTransform:{exposure:.85}});
			
			_scroller.startDrag(false, new Rectangle(0, 0, 0, _maxH));
			_mouseDown = true;
		}
		
		private function onMouseUpHandler(event:MouseEvent):void {
			//trace("onMouseMoveHandler value is " + value);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			
			TweenLite.killTweensOf(_scroller);
			TweenLite.to(_scroller, .1, {colorTransform:{exposure:1}});
			
			_mouseDown = false;
			_scroller.stopDrag();
		}

		private function onMouseMoveHandler(event:MouseEvent):void {
			//trace("onMouseMoveHandler");
			
			if (_mouseDown) {
				//trace("INFO ScrollBar :: value is " + value);
				dispatchEvent(new Event(SCROLL));
			}
		}
		
		private function onGutterClickHandler(event:MouseEvent):void {
			_scroller.y = Math.max(Math.min(_maxY, mouseY), 0);
			dispatchEvent(new Event(SCROLL));
		}
	}//c
}//p