package com.ges.ui {
	import com.base.utils.TextUtils;
	
	import com.ges.assets.AnswerButtonAsset;
	import com.ges.assets.AnswerButtonSmAsset;
	import com.ges.data.AnswerData;
	import com.ges.manager.StyleManager;

	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class AnswerButton extends Sprite 
	{
		private var _asset					:MovieClip;
		private var _data					:AnswerData;
		
		private var _styleManager			:StyleManager;
		
		private var _txtMC					:MovieClip;
		private var _tightLeadingFormat		:TextFormat;
		private var _regularLeadingFormat	:TextFormat;
		
		private var _isAnswer				:Boolean = false;
		public function get isAnswer():Boolean
		{
			return _isAnswer;
		}

		private var _selected				:Boolean = false;
		public function set selected(b:Boolean):void
		{
			if (_selected != b) {
				_selected = b;
				update();
			}
		}
		public function get selected():Boolean
		{
			return _selected;
		}
		
		public var index					:uint = 0;

		private var _isSpecial				:Boolean = false;	//true:for 6 button case
		public function get isSpecial():Boolean
		{
			return _isSpecial;
		}
		
		private var _answerID				:String;
		public function get answerID():String
		{
			return _data.id;
		}
		
		
		public function AnswerButton(pIndex:uint, pData:AnswerData) 
		{
			index = pIndex;
			_data = pData;
			
			_styleManager = StyleManager.getInstance();
			
			_isAnswer = _data.isAnswer;
			_isSpecial = _data.isSpecial;
			
			mouseChildren = false;
			addEventListener(Event.ADDED_TO_STAGE, configUI);
		}

		private function configUI(e:Event):void
		{		
			removeEventListener(Event.ADDED_TO_STAGE, configUI);
//			trace("INFO AnswerButton :: configUI, _data.answer is " + _data.answer);

			if (!_isSpecial) {
				_asset = new AnswerButtonAsset();
				addChild(_asset);
				
				_txtMC = _asset.txtMC;
				_txtMC.textField.autoSize = TextFieldAutoSize.CENTER;
				_txtMC.textField.styleSheet = _styleManager.styleSheet;
				_txtMC.textField.htmlText = _data.answer;
				//				_txtMC.textField.y = _data.textY;

				_txtMC.textField.y = -Math.floor(_txtMC.textField.textHeight/2 + 1);
				
//				trace("INFO AnswerButton :: _data.answer is - " + _data.answer);
			} else {
				_asset = new AnswerButtonSmAsset();
				addChild(_asset);
				
				_txtMC = _asset.txtMC;
				_txtMC.gotoAndStop(_data.index + 1);
			}
			
			update();
		}
		
		private function update():void
		{
			mouseEnabled = buttonMode = !_selected;
			if (!_selected) {
				_asset.gotoAndStop(1);
			} else if (_isAnswer) {
				_asset.gotoAndStop(2);
			} else {
				_asset.gotoAndStop(3);
			}
		}
		
		public function deactivate():void
		{
			mouseEnabled = buttonMode = false;
		}
		
		public function destroy():void
		{
			removeChild(_asset);
			_asset = null;
		}
	}//c
}//p