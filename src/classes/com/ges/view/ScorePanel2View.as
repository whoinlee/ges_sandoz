package com.ges.view {
	import com.ges.assets.ScorePanel2Asset;
	import com.ges.data.UserData;
	import com.ges.manager.*;
	import com.ges.events.GameEvent;
	
	import com.greensock.TweenLite;

	import flash.events.MouseEvent;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.utils.setInterval;
	import flash.utils.clearInterval;
	
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="isi", type="com.ges.events.GameEvent")]
	[Event(name="ref2", type="com.ges.events.GameEvent")]
	[Event(name="quit", type="com.ges.events.GameEvent")]
	public class ScorePanel2View extends BaseView {
		
		private static const CORRECT_COLOR		:Number = 0x189418;
		private static const INCORRECT_COLOR	:Number = 0xb91f1f;
		
		private var _asset						:ScorePanel2Asset;
		
		private var _isiBt						:MovieClip;
		private var _refBt						:MovieClip;
		private var _quitBt						:MovieClip;
		
		private var _scoreField					:TextField;
		private var _timeField					:TextField;
		private var _scoreOffsetField			:TextField;
		
		private var _currentUser				:UserData;
		
		private var _updateTimeID				:Number;
		private var _secPassed					:Number = 0;
		private var _gameTimeout				:uint;
		private var _currentScore				:int;
		
		private var _isTimerStopped				:Boolean = false;
		
		private var _secLeft					:uint;
		public function get secLeft():uint
		{
			_secLeft = _gameTimeout - _secPassed;
			return _secLeft;
		}

		
		public function ScorePanel2View() {
			super();
			viewName = "ScorePanel2";
		}
		
		override protected function configUI():void
		{
			_controller.registerView(Controller.SCOREPANEL2_VIEW, this);
			_currentUser = _dataModel.currentUser;

			_asset = new ScorePanel2Asset();
			addChild(_asset);
			
			build();
		}
		
		override public function build():void
		{
			_isiBt = _asset.isiBt;
			_refBt = _asset.refBt;
			_quitBt = _asset.quitBt;
			
			_isiBt.buttonMode = _isiBt.mouseEnabled = true;
			_refBt.buttonMode = _refBt.mouseEnabled = true;
			_quitBt.buttonMode = _quitBt.mouseEnabled = true;
			_isiBt.mouseChildren = _refBt.mouseChildren = _quitBt.mouseChildren = false;
			
			_isiBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_refBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_quitBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			
			_isiBt.addEventListener(MouseEvent.MOUSE_UP, showISIRequested);
			_refBt.addEventListener(MouseEvent.MOUSE_UP, showRefRequested);
			_quitBt.addEventListener(MouseEvent.MOUSE_UP, onQuitRequested);
			
			_isiBt.addEventListener(MouseEvent.MOUSE_OUT, showISIRequested);
			_refBt.addEventListener(MouseEvent.MOUSE_OUT, showRefRequested);
			_quitBt.addEventListener(MouseEvent.MOUSE_OUT, onQuitRequested);
			
			_scoreField = _asset.scoreField;
			_timeField = _asset.timeField;
			_scoreOffsetField = _asset.scoreOffsetField;
			
			reset();
			deactivateRefBt();
			hideRefBt();
		}
		
		public function reset():void
		{
			_currentScore = _currentUser.score;
			_gameTimeout = _dataModel.gameTimeout;
			
			_scoreField.text = _currentScore + "";
			_timeField.text = _gameTimeout + "";
			_scoreOffsetField.text = "";
			
			_updateTimeID = setInterval(updateTime, 1000);
			_secPassed = 0;
			_secLeft = _gameTimeout;
			
			_isTimerStopped = false;
		}
		
		public override function destroy():void
		{
			_isiBt.removeEventListener(MouseEvent.MOUSE_DOWN, showISIRequested);
			_refBt.removeEventListener(MouseEvent.MOUSE_DOWN, showRefRequested);
			_quitBt.removeEventListener(MouseEvent.MOUSE_DOWN, onQuitRequested);
			
			_isiBt.removeEventListener(MouseEvent.MOUSE_UP, showISIRequested);
			_refBt.removeEventListener(MouseEvent.MOUSE_UP, showRefRequested);
			_quitBt.removeEventListener(MouseEvent.MOUSE_UP, onQuitRequested);
			
			_isiBt.removeEventListener(MouseEvent.MOUSE_OUT, showISIRequested);
			_refBt.removeEventListener(MouseEvent.MOUSE_OUT, showRefRequested);
			_quitBt.removeEventListener(MouseEvent.MOUSE_OUT, onQuitRequested);
			
			clearInterval(_updateTimeID);
			
			if (_asset) {
				removeChild(_asset);
				_asset = null;
			}
		}
				
		public function updateScore(scoreOffset:int):void
		{
//			trace("INFO ScorePanel2View :: updateScore");
			
			_currentScore += scoreOffset;
			_currentUser.score = _currentScore;
			_scoreField.text = _currentScore + "";
			_currentUser.score = _currentScore;
			
			if (scoreOffset > 0) {
				_scoreOffsetField.textColor = CORRECT_COLOR;
				_scoreOffsetField.text = "+" + scoreOffset + " pts";
			} else {
				_scoreOffsetField.textColor = INCORRECT_COLOR;
				_scoreOffsetField.text = scoreOffset + " pts";
			}
		}
		
		public function deactivateRefBt():void
		{
			_refBt.buttonMode = _refBt.mouseEnabled = false;
		}
		
		public function activateRefBt():void
		{
			_refBt.buttonMode = _refBt.mouseEnabled = true;
		}
		
		public function hideRefBt():void
		{
			_refBt.visible = false;
		}
		
		public function showRefBt():void
		{
			_refBt.visible = true;
		}
		
		public function deactivateButtons():void
		{
			_refBt.buttonMode = _refBt.mouseEnabled = false;
			_isiBt..buttonMode = _isiBt..mouseEnabled = false;
			_quitBt.buttonMode = _quitBt.mouseEnabled = false;
		}
		
		public function activateButtons():void
		{
			_refBt.buttonMode = _refBt.mouseEnabled = true;
			_isiBt..buttonMode = _isiBt..mouseEnabled = true;
			_quitBt.buttonMode = _quitBt.mouseEnabled = true;
		}
				
		public function resetOffset():void
		{
			_scoreOffsetField.text = "";
		}
		
		public function killTimer():void
		{
			clearInterval(_updateTimeID);
		}
		
		public function stopTimer():void
		{
			_isTimerStopped = true;
			clearInterval(_updateTimeID);
		}
		
		public function restartTimer():void
		{
			if (_isTimerStopped) {
				_isTimerStopped = false;
				updateTime();
				_updateTimeID = setInterval(updateTime, 1000);
			}
		}
		
		private function updateTime():void
		{
//			trace("INFO ScorePanel2View :: updateTime");
			_secPassed += 1;
			_secLeft = _gameTimeout - _secPassed;
			_timeField.text = _secLeft + "";
			if (_secLeft == 0) onTimeOut();
		}

		private function onTimeOut():void
		{
			clearInterval(_updateTimeID);
			dispatchEvent(new GameEvent(GameEvent.TIMEOUT));
			
			_isTimerStopped = false;
		}
		
		public function onQuit():void
		{
			clearInterval(_updateTimeID);
			_timeField.text = "0";
			
			_isTimerStopped = false;
		}
		

		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onButtonPressed(e:MouseEvent):void
		{
//			trace("INFO ScorePanel2View :: onButtonPressed, e.target is " + e.target);
			
			_soundController.playClick();
			
			var target:MovieClip = e.target as MovieClip;
			target.isDown = true;
			TweenLite.killTweensOf(target);
			TweenLite.to(target, .1, {colorTransform:{exposure:.85}}); 
		}
		
		private function showISIRequested(e:MouseEvent):void
		{
//			trace("INFO ScorePanel2View :: showISIRequested");

			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 
			
//				_soundController.playClick();
				dispatchEvent(new GameEvent(GameEvent.ISI));
			}
		}
		
		private function showRefRequested(e:MouseEvent):void
		{
//			trace("INFO ScorePanel2View :: showRefRequested");

			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 

//				_soundController.playClick();
				dispatchEvent(new GameEvent(GameEvent.REF));
			}
		}
		
		private function onQuitRequested(e:MouseEvent = null):void
		{
//			trace("INFO ScorePanel2View :: onQuitRequested");
			
			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 
			
//				_soundController.playClick();
				dispatchEvent(new GameEvent(GameEvent.QUIT));
			}
		}
		
	}//c
}//p