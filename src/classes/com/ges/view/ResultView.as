package com.ges.view {
	import com.ges.assets.ResultContent;
	import com.ges.events.GameEvent;
	import com.ges.manager.*;
	import com.ges.data.UserData;
	import com.ges.ui.KeyboardPane;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	import flash.utils.setTimeout;
	import flash.utils.clearTimeout;
	
	import com.greensock.TweenLite;
	
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="restart", type="com.ges.events.GameEvent")]
	[Event(name="isi", type="com.ges.events.GameEvent")]
	[Event(name="quit", type="com.ges.events.GameEvent")]
	public class ResultView extends BaseView {

		private static const KEYBOARD_PANE_X	:Number = 480;
		private static const KEYBOARD_PANE_Y	:Number = 690;
		
		private var _content					:ResultContent;
		
		private var _tryAgainBt					:MovieClip;
		private var _isiBt						:MovieClip;
		private var _quitBt						:MovieClip;
		
		private var _yourScoreMC				:MovieClip;
		private var _scoreHolder				:MovieClip;
		private var _scoreNextText				:MovieClip;
		
		private var _currentPageID				:uint = 1;
		private var _isTop10					:Boolean = false;
		
		private var _keyboardPane				:KeyboardPane;
		
		private var _timeoutID					:Number;
		private var _leaderboardTimeout			:uint;
		
		
		public function ResultView(pPageID:uint = 1) 
		{
			super();
			viewName = "Result";
			
			_currentPageID = pPageID;
			_isTop10 = (pPageID == 1)? false:true;
		}
		
		override protected function configUI() : void
		{
			_controller.registerView(Controller.RESULT_VIEW, this);
			
			_leaderboardTimeout = _dataModel.leaderboardTimeout;
			
			_content = new ResultContent();
			addChild(_content);
			
			_scoreHolder = _content.scoreHolder;
			_yourScoreMC = _content.yourScoreMC;
			
			_tryAgainBt = _content.tryAgainBt;
			_isiBt = _content.isiBt;
			_quitBt = _content.quitBt;
			
			_tryAgainBt.buttonMode = _tryAgainBt.mouseEnabled = true;
			_isiBt.buttonMode = _isiBt.mouseEnabled = true;
			_quitBt.buttonMode = _quitBt.mouseEnabled = true;
			_tryAgainBt.mouseChildren = _isiBt.mouseChildren = _quitBt.mouseChildren = false;
			
			_tryAgainBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_isiBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_quitBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			
			_tryAgainBt.addEventListener(MouseEvent.MOUSE_OUT, onRestartRequested);
			_isiBt.addEventListener(MouseEvent.MOUSE_OUT, showISIRequested);
			_quitBt.addEventListener(MouseEvent.MOUSE_OUT, onQuitRequested);
			
			_tryAgainBt.addEventListener(MouseEvent.MOUSE_UP, onRestartRequested);
			_isiBt.addEventListener(MouseEvent.MOUSE_UP, showISIRequested);
			_quitBt.addEventListener(MouseEvent.MOUSE_UP, onQuitRequested);
			
			buildPage(_currentPageID);
			playGameEnd();
		}
		
		public override function destroy():void
		{
			clearTimeout(_timeoutID);
			
			_tryAgainBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_isiBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_quitBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			
			_tryAgainBt.removeEventListener(MouseEvent.MOUSE_UP, onRestartRequested);
			_isiBt.removeEventListener(MouseEvent.MOUSE_UP, showISIRequested);
			_quitBt.removeEventListener(MouseEvent.MOUSE_UP, onQuitRequested);
			
			_tryAgainBt.removeEventListener(MouseEvent.MOUSE_OUT, onRestartRequested);
			_isiBt.removeEventListener(MouseEvent.MOUSE_OUT, showISIRequested);
			_quitBt.removeEventListener(MouseEvent.MOUSE_OUT, onQuitRequested);
			
			if (_keyboardPane) {
				_keyboardPane.destroy();
				removeChild(_keyboardPane);
				_keyboardPane = null;
			}
			
			if (_content) {
				removeChild(_content);
				_content = null;
			}
		}
		
		private function buildPage(pageID:uint):void
		{
			_content.gotoAndStop(pageID);
			switch (pageID) {
				case 1:
					_tryAgainBt.visible = true;
					_tryAgainBt.mouseEnabled = _tryAgainBt.buttonMode = true;
					updateScores();
					_scoreHolder.visible = true;
					if (_yourScoreMC.visible) {
						//-- hasn't been to the 2nd page, i.e. not in the top 10
						_yourScoreMC.scoreField.text = _dataModel.currentUser.score + ".";
					}
					//05/17 update
					_timeoutID = setTimeout(onTimeout, _leaderboardTimeout * 1000);
					break;
				case 2:
					_content.scoreField.autoSize = TextFieldAutoSize.LEFT;
					_content.scoreField.text = _dataModel.currentUser.score + "";
					_content.scoreNextText.x = _content.scoreField.x + _content.scoreField.textWidth + 8;
					_yourScoreMC.visible = false;
					_tryAgainBt.visible = false;
					_tryAgainBt.mouseEnabled = _tryAgainBt.buttonMode = false;
					_scoreHolder.visible = false;
					
					_keyboardPane = new KeyboardPane();
					_keyboardPane.x = KEYBOARD_PANE_X;
					_keyboardPane.y = KEYBOARD_PANE_Y;
					_keyboardPane.addEventListener("done", onKeyBoardDone);
					addChild(_keyboardPane);
			}
		}
		
		private function updateScores():void
		{
			var top10Arr:Array = _dataModel.top10Arr;
			var total:uint = top10Arr.length;
			if (!_isTop10) {
				for (var i=0; i<total; i++) {
					_scoreHolder["scoreMC" + i].nameField.text = top10Arr[i].acro;
					_scoreHolder["scoreMC" + i].scoreField.text = top10Arr[i].score;
					_scoreHolder["scoreMC" + i].ptsMC.visible = true;
				}
				for (var j=total; j<10; j++) {
					_scoreHolder["scoreMC" + j].nameField.text = "";
					_scoreHolder["scoreMC" + j].scoreField.text = "";
					_scoreHolder["scoreMC" + j].ptsMC.visible = false;
				}
			} else {
				var currentUser:UserData = _dataModel.currentUser;
				for (var l=0; l<total; l++) {
					var thisUser:UserData = top10Arr[l];
					if (thisUser == currentUser) {
						_scoreHolder["scoreMC" + l].gotoAndStop(2);
						_scoreHolder["scoreMC" + l].nameField.autoSize = TextFieldAutoSize.LEFT;
						_scoreHolder["scoreMC" + l].scoreField.autoSize = TextFieldAutoSize.LEFT;
						_scoreHolder["scoreMC" + l].nameField.text = thisUser.acro;
						_scoreHolder["scoreMC" + l].scoreField.text = thisUser.score;
						_scoreHolder["scoreMC" + l].scoreField.x = Math.floor(_scoreHolder["scoreMC" + l].nameField.x + _scoreHolder["scoreMC" + l].nameField.textWidth + 18);
						_scoreHolder["scoreMC" + l].ptsMC.x = Math.floor(_scoreHolder["scoreMC" + l].scoreField.x + _scoreHolder["scoreMC" + l].scoreField.textWidth + 8);
					} else {
						_scoreHolder["scoreMC" + l].gotoAndStop(1);
						_scoreHolder["scoreMC" + l].ptsMC.visible = true;
						_scoreHolder["scoreMC" + l].nameField.text = thisUser.acro;
						_scoreHolder["scoreMC" + l].scoreField.text = thisUser.score;
					}
				}
				for (var m=total; m<10; m++) {
					_scoreHolder["scoreMC" + m].gotoAndStop(1);
					_scoreHolder["scoreMC" + m].nameField.text = "";
					_scoreHolder["scoreMC" + m].scoreField.text = "";
					_scoreHolder["scoreMC" + m].ptsMC.visible = false;
				}
			}
		}
		
		private function playGameEnd(pVolume:Number = .5):void
		{
			_soundController.playGameEnd(pVolume);
		}

		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onButtonPressed(e:MouseEvent):void
		{
//			trace("INFO ResultView :: onButtonPressed, e.target is " + e.target);
			
			_soundController.playClick();
			
			var target:MovieClip = e.target as MovieClip;
			target.isDown = true;
			TweenLite.killTweensOf(target);
			TweenLite.to(target, .1, {colorTransform:{exposure:.85}}); 
		}

		private function onRestartRequested(e:MouseEvent):void
		{
//			trace("INFO ResultView :: onRestartRequested");

			clearTimeout(_timeoutID);
			
			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 

				dispatchEvent(new GameEvent(GameEvent.RESTART));
			}
		}
		
		private function showISIRequested(e:MouseEvent):void
		{
//			trace("INFO ResultView :: showISIRequested");
			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 

				dispatchEvent(new GameEvent(GameEvent.ISI));
			}
		}
		
		private function onQuitRequested(e:MouseEvent):void
		{
//			trace("INFO ResultView :: onQuitRequested");

			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 
				
				clearTimeout(_timeoutID);
				dispatchEvent(new GameEvent(GameEvent.QUIT));
			}
		}
		
		private function onKeyBoardDone(e: Event):void
		{
			//05/17 update
			_timeoutID = setTimeout(onTimeout, _leaderboardTimeout * 1000);
					
			_dataModel.addUser();
			_isTop10 = true;
			
			_content.gotoAndStop(1);

			_tryAgainBt.visible = true;
			_tryAgainBt.mouseEnabled = _tryAgainBt.buttonMode = true;
			
			_scoreHolder.visible = true;
			updateScores();
			
			//05/19 update
			if (_keyboardPane) {
				_keyboardPane.destroy();
				removeChild(_keyboardPane);
				_keyboardPane = null;
			}
		}
		
		private function onTimeout():void
		{
			trace("INFO ResultView :: onTimeout");
			
			clearTimeout(_timeoutID);
			dispatchEvent(new GameEvent(GameEvent.QUIT));
		}
	}//c
}//p