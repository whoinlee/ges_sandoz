package com.ges.view {
	import com.ges.assets.ScoreMC;
	import com.ges.assets.ScorePanel1Asset;
	import com.ges.manager.*;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;
	import com.greensock.easing.Quint;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.display.Graphics;
	import flash.events.Event;
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class ScorePanel1View extends BaseView {
		
		public static const BKG_W				:Number = 1920;
		public static const BKG_H				:Number = 132;
		public static const BKG_EXTRA_H			:Number = 30;
		
		private static const scoreMC_xLOC		:Array = [333, 786, 1239];
		private static const scoreMC_yLOC		:Number = 41;
		
		private var _bkg						:Shape;
		private var _asset						:ScorePanel1Asset;
		
		private var _top3Arr					:Array;
		private var _scoreMC0					:ScoreMC;
		private var _scoreMC1					:ScoreMC;
		private var _scoreMC2					:ScoreMC;
		
		
		public function ScorePanel1View() {
			super();
			viewName = "ScorePanel1";
		}
		
		override protected function configUI():void
		{
			_controller.registerView(Controller.SCOREPANEL1_VIEW, this);
			
			build();
		}
		
		override public function build():void
		{
			_bkg = new Shape();
			var bg:Graphics = _bkg.graphics;
			bg.beginFill(0xe7e7e7, 1);
			bg.drawRect(0, 0, BKG_W, BKG_H + BKG_EXTRA_H);
			bg.endFill();
			addChild(_bkg);
			
			_asset = new ScorePanel1Asset();
			addChild(_asset);
			
			_top3Arr = _dataModel.top3Arr;
			var total:uint = _top3Arr.length;
			for (var i = 0; i<total; i++) {
				this["_scoreMC" + i] = new ScoreMC();
				var thisMC:ScoreMC = this["_scoreMC" + i];
				addChild(thisMC);
				thisMC.x = scoreMC_xLOC[i];
				thisMC.y = scoreMC_yLOC;
				thisMC.nameField.text = _top3Arr[i].acro;
				thisMC.scoreField.text = _top3Arr[i].score;
			}
		}
		
		override public function destroy():void
		{
			if (_scoreMC0) removeChild(_scoreMC0);
			if (_scoreMC1) removeChild(_scoreMC1);
			if (_scoreMC2) removeChild(_scoreMC2);
			
			_scoreMC0 = null;
			_scoreMC1 = null;
			_scoreMC2 = null;
		}
		
	}//c
}//p