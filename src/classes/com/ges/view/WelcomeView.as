package com.ges.view {
	import com.ges.assets.WelcomeContent;
	import com.ges.events.GameEvent;
	import com.ges.manager.*;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import com.greensock.TweenLite;
//	import com.greensock.easing.Quad;
//	import com.greensock.easing.Quint;
//	import com.greensock.plugins.TintPlugin;
//	import com.greensock.plugins.TweenPlugin;
	
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="start", type="com.ges.events.GameEvent")]
	[Event(name="isi", type="com.ges.events.GameEvent")]
	[Event(name="ref1", type="com.ges.events.GameEvent")]
	[Event(name="quit", type="com.ges.events.GameEvent")]
	public class WelcomeView extends BaseView {

		private var _content					:WelcomeContent;
		
		private var _startBt					:MovieClip;
		private var _isiBt						:MovieClip;
		private var _refBt						:MovieClip;
		private var _quitBt						:MovieClip;
		
		private var _nameField					:TextField;
		
		
		public function WelcomeView() {
			super();
			viewName = "Welcome";
		}
		
		override protected function configUI() : void
		{
			_controller.registerView(Controller.WELCOME_VIEW, this);
			
			_content = new WelcomeContent();
			addChild(_content);
			
			_startBt = _content.startBt;
			_isiBt = _content.isiBt;
			_refBt = _content.refBt;
			_quitBt = _content.quitBt;
			
			_startBt.buttonMode = _startBt.mouseEnabled = true;
			_isiBt.buttonMode = _isiBt.mouseEnabled = true;
			_refBt.buttonMode = _refBt.mouseEnabled = true;
			_quitBt.buttonMode = _quitBt.mouseEnabled = true;
			_startBt.mouseChildren = _isiBt.mouseChildren = _refBt.mouseChildren = _quitBt.mouseChildren = false;
			
			_nameField = _content.nameField;
			_nameField.text = "HELLO " + _dataModel.currentUser.name.toUpperCase();
			
			_startBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_isiBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
//			_refBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_quitBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			
			_startBt.addEventListener(MouseEvent.MOUSE_UP, onStartRequested);
			_isiBt.addEventListener(MouseEvent.MOUSE_UP, showISIRequested);
//			_refBt.addEventListener(MouseEvent.MOUSE_UP, showRefRequested);
			_quitBt.addEventListener(MouseEvent.MOUSE_UP, onQuitRequested);
			
			_startBt.addEventListener(MouseEvent.MOUSE_OUT, onStartRequested);
			_isiBt.addEventListener(MouseEvent.MOUSE_OUT, showISIRequested);
//			_refBt.addEventListener(MouseEvent.MOUSE_OUT, showRefRequested);
			_quitBt.addEventListener(MouseEvent.MOUSE_OUT, onQuitRequested);
			
			
			deactivateRefBt();
			deactivateStart();
		}
		
		public override function destroy():void
		{
			_startBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_isiBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
//			_refBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_quitBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			
			_startBt.removeEventListener(MouseEvent.MOUSE_UP, onStartRequested);
			_isiBt.removeEventListener(MouseEvent.MOUSE_UP, showISIRequested);
//			_refBt.removeEventListener(MouseEvent.MOUSE_UP, showRefRequested);
			_quitBt.removeEventListener(MouseEvent.MOUSE_UP, onQuitRequested);
			
			_startBt.removeEventListener(MouseEvent.MOUSE_OUT, onStartRequested);
			_isiBt.removeEventListener(MouseEvent.MOUSE_OUT, showISIRequested);
//			_refBt.removeEventListener(MouseEvent.MOUSE_OUT, showRefRequested);
			_quitBt.removeEventListener(MouseEvent.MOUSE_OUT, onQuitRequested);
			
			if (_content) {
				removeChild(_content);
				_content = null;
			}
		}
		
		private function deactivateRefBt():void
		{
			_refBt.buttonMode = _refBt.mouseEnabled = false;
//			_refBt.alpha = .5;
			_refBt.visible = false;
		}
		
		private function activateRefBt():void
		{
			_refBt.buttonMode = _refBt.mouseEnabled = true;
//			_refBt.alpha = 1;
			_refBt.visible = true;
		}
		
		public function deactivateStart():void
		{
			_startBt.buttonMode = _startBt.mouseEnabled = false;
			
			_startBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_startBt.removeEventListener(MouseEvent.MOUSE_UP, onStartRequested);
			_startBt.removeEventListener(MouseEvent.MOUSE_OUT, onStartRequested);
		}
		
		public function activateStart():void
		{
			_startBt.buttonMode = _startBt.mouseEnabled = true;
			
			_startBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
			_startBt.addEventListener(MouseEvent.MOUSE_UP, onStartRequested);
			_startBt.addEventListener(MouseEvent.MOUSE_OUT, onStartRequested);
		}
		

		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onButtonPressed(e:MouseEvent):void
		{
			trace("INFO WelcomeView :: onButtonPressed");
			
			_soundController.playClick();
			
			var target:MovieClip = e.target as MovieClip;
			target.isDown = true;
			TweenLite.killTweensOf(target);
			TweenLite.to(target, .1, {colorTransform:{exposure:.85}}); 
		}
		
//		private function onButtonOut(e:MouseEvent):void
//		{
//			trace("INFO WelcomeView :: onButtonOut");
//
//			var target:MovieClip = e.target as MovieClip;
//			TweenLite.killTweensOf(target);
//			TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 
//		}
		
		private function onStartRequested(e:MouseEvent):void
		{
			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 
			
				trace("INFO WelcomeView :: onStartRequested");
//			_soundController.playClick();		
				dispatchEvent(new GameEvent(GameEvent.START));
			}
		}
		
		private function showISIRequested(e:MouseEvent):void
		{
			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 
				
				trace("INFO WelcomeView :: showISIRequested");
//			_soundController.playClick();
				dispatchEvent(new GameEvent(GameEvent.ISI));
			}
		}
		
		private function showRefRequested(e:MouseEvent):void
		{
			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 
				
				trace("INFO WelcomeView :: showRefRequested");
//			_soundController.playClick();
				dispatchEvent(new GameEvent(GameEvent.REF));
			}
		}
		
		private function onQuitRequested(e:MouseEvent):void
		{
			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 
				
				trace("INFO WelcomeView :: onQuitRequested");
//			_soundController.playClick();
				dispatchEvent(new GameEvent(GameEvent.QUIT));
			}
		}
	}//c
}//p