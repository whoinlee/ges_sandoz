package com.ges.view {
	import com.ges.assets.GameContent;
	import com.ges.data.*;
	import com.ges.events.GameEvent;
	import com.ges.manager.*;
	import com.ges.ui.AnswerButton;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.utils.clearInterval;
	import flash.utils.clearTimeout;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;

	
	
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="updateScore", type="com.ges.events.GameEvent")]
	[Event(name="backToCategory", type="flash.events.Event")]
	public class GameView extends BaseView {

		private static const BUTTON_X			:Number = 554;
		private static const BUTTON_Y_OFFSET	:Number = 140;	
		private static const SM_BUTTON_X_OFFSET	:Number = 411;
		private static const SM_BUTTON_Y_OFFSET	:Number = 187;
		
		private static const ANSWER_TIMEOUT		:uint = 15;			//15 sec
		
		private var _data						:QuizData;
		private var _content					:GameContent;
		
		private var _categoryField				:TextField;
		private var _scoreField					:TextField;
		private var _questionField				:TextField;
		private var _counter					:MovieClip;
		
		private var _buttonHolder				:Sprite;
		private var _buttonArr					:Array = [];
		
		private var _answerIndex				:uint;
		
		private var _answerSelected				:Boolean = false;
		private var _isSpecial					:Boolean = false;	//true:for 6 button case
		
		private var _trademarks					:MovieClip;
		private var _refID						:uint = 11;
		
		private var _secPassed					:uint;
		private var _secLeft					:uint;
		
		private var _updateCounterID			:Number;
		private var _checkSecPassedID			:Number;
		private var _backToCategoryID			:Number;
		
		private var _isCounterPaused			:Boolean = false;
		private var _isTimerStopped				:Boolean = false;
	
		
		public function set data(d:QuizData)	:void
		{
			trace("INFO GameView :: set data");
			_data = d;
			update();
		}
		
		public function get refID()				:uint
		{
			return _refID;
		}
		

		public function GameView(pQuizData:QuizData) {
			super();
			viewName = "Game";
			_data = pQuizData;
		}
		
		override protected function configUI() : void
		{
			trace("INFO GameView :: configUI");
//			TextUtils.traceFonts();
			
			_controller.registerView(Controller.GAME_VIEW, this);
			
			_content = new GameContent();
			addChild(_content);
			
			_categoryField = _content.categoryField;
			_scoreField = _content.scoreField;
			_questionField = _content.questionField;
			_questionField.styleSheet = _styleManager.styleSheet;
			
			_counter = _content.counter;
			_counter.circle.stop();
			_counter.visible = false;
			
			_trademarks = _content.trademarks;
			
			_buttonHolder = new Sprite();
			_content.addChild(_buttonHolder);
			_buttonHolder.x = BUTTON_X;
			
			_buttonArr = [];
			
			update();
			startGame();
		}
		
		private function update():void
		{
			trace("INFO GameView :: update");
			
			_isSpecial = _data.isSpecial;
			_refID = _data.refTypeID;
			
			_categoryField.autoSize = TextFieldAutoSize.LEFT;
			_categoryField.text = _data.category.toUpperCase();
			_categoryField.x = _data.categoryX;
			
			_scoreField.text = _data.points + " pts";
			_scoreField.x = _categoryField.x + _categoryField.textWidth + 40;
			
			_questionField.autoSize = TextFieldAutoSize.LEFT;
			_questionField.styleSheet = _styleManager.styleSheet;
			_questionField.htmlText = _data.question;
			_questionField.y = _data.questionY;
			
			_buttonHolder.y = _data.answerY;
			var total:uint = _buttonArr.length;
			if (total > 0) {
				//-- if there are buttons previously constructed
				for (var i:uint=0; i<total; i++) {
					var thisButton:AnswerButton = _buttonArr[i];
					thisButton.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
					thisButton.removeEventListener(MouseEvent.MOUSE_UP, onAnswerSelected);
					_buttonHolder.removeChild(_buttonArr[i]);
					_buttonArr[i] = null;
				}	
			}

			_buttonArr = [];
			_answerIndex = _data.answerIndex;
			var answerDataArr:Array = _data.answerDataArr;
			var numOfAnswers:uint = answerDataArr.length;
			if (_isSpecial) {
				//-- special case with a different layout, more than 5 answer case
				for (var j:uint=0; j<numOfAnswers; j++) {
					var answerButtonSm:AnswerButton = new AnswerButton(j, answerDataArr[j]);
					var row:uint = Math.floor(j/2);
					var col:uint = j%2; 
					_buttonHolder.addChild(answerButtonSm);
					answerButtonSm.x = col*SM_BUTTON_X_OFFSET;
					answerButtonSm.y = row*SM_BUTTON_Y_OFFSET;
					answerButtonSm.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
					answerButtonSm.addEventListener(MouseEvent.MOUSE_UP, onAnswerSelected);
					_buttonArr.push(answerButtonSm);
				}
			} else {
				for (var k:uint=0; k<numOfAnswers; k++) {
					var answerButton:AnswerButton = new AnswerButton(k, answerDataArr[k]);
					_buttonHolder.addChild(answerButton);
					answerButton.y = k*BUTTON_Y_OFFSET;
					answerButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
					answerButton.addEventListener(MouseEvent.MOUSE_UP, onAnswerSelected);
					_buttonArr.push(answerButton);
				}
			}
			
			_trademarks.trademark1.visible = _trademarks.trademark2.visible = _trademarks.trademark3.visible = _trademarks.trademark4.visible = false;
			_trademarks.trademark0.visible = false;
			if (_data.trademarkArr) {
				var trademarkIndexArr:Array = _data.trademarkArr;
				var trademarkTotal:uint = trademarkIndexArr.length;
				var selectedTrademarkArr:Array = [];
				for (var l:uint=0; l<trademarkTotal; l++) {
					var index:uint = trademarkIndexArr[l];
//					trace("index: " + index);
					_trademarks["trademark" + index].visible = true;
					selectedTrademarkArr.push(_trademarks["trademark" + index]);
				}//for
				
				//-- updated 5/17
				var selectedTotal:uint = selectedTrademarkArr.length;
				var lastY:Number = 60;
				var count:uint = 0;
				for (var s:uint=selectedTotal; s>0; s--) {
					var thisMark:MovieClip = selectedTrademarkArr[s-1];
					thisMark.y = lastY - count*20;
//					trace(s + ": thisMark.y, " + thisMark.y);
					count++;
				}//for
				
				//-- special case: 6 answer
				if (_trademarks.trademark0.visible) {
					_trademarks.trademark0.y = -24;
				}
			}//if
	
		}
		
		private function startGame():void
		{
			trace("INFO GameView :: startGame");
			
			_secPassed = 0;
			_checkSecPassedID = setInterval(checkSecPassed, 1000);
		}
		
		private function checkSecPassed():void
		{
			_secPassed++;
			
			trace("INFO GameView :: checkSecPassed, " + _secPassed);
			
			_secLeft = ANSWER_TIMEOUT - _secPassed;
			
			if (_secLeft == 5 && !_counter.visible) {
				resetCounter();
				showCounter();
			}
			
			if (_secLeft == 0) {
				if (!_answerSelected) {
					_answerSelected = true;
					
					playInCorrect();
					showCorrectAnswer();
					deactivateButtons();
					
					var scoreOffset:uint = _data.points;
					scoreOffset *= -1;
					dispatchEvent(new GameEvent(GameEvent.UPDATE_SCORE, scoreOffset));
					
					clearTimeout(_backToCategoryID);
					_backToCategoryID = setTimeout(backToCategory, _dataModel.backToCategoryDelay * 1000);
				}
				hideCounter();
			}
		}
		
		private function showCounter():void
		{
//			trace("INFO GameView :: showCounter");
			

//			_secLeft = 5;	//5/22
			
			resetCounter();	//5/22
			if (!_counter.visible) {
				_counter.alpha = 0;
				_counter.circle.play();
				_counter.visible = true;
				TweenLite.killTweensOf(_counter);
				TweenLite.to(_counter, .3, {alpha:1, ease:Quad.easeOut});
				
				clearInterval(_updateCounterID);
				_updateCounterID = setInterval(updateCounter, 1000);
			}
		}
		
		private function hideCounter():void
		{
			clearInterval(_updateCounterID);
			
			_counter.circle.stop();
			TweenLite.killTweensOf(_counter);
			TweenLite.to(_counter, .3, {width:1, height:1, alpha:0, ease:Quad.easeOut, onComplete:function(){_counter.visible = false;}});
		}
		
		private function resetCounter():void
		{
//			trace("INFO GameView :: resetCounter");
			
			_counter.xScale = _counter.yScale = 1;
			_counter.gotoAndStop(1);
			_counter.circle.gotoAndStop(1);
		}
		
		private function updateCounter():void
		{
			trace("INFO GameView :: udpateCounter");
			
//			_secLeft--;
			if (_secLeft <0) _secLeft = 0;
			
			var nextFrame:uint = 6 - _secLeft;
			_counter.gotoAndStop(nextFrame);
			
//			if (_secLeft == 0) {
//				if (!_answerSelected) {
//					_answerSelected = true;
//					
//					playInCorrect();
//					showCorrectAnswer();
//					deactivateButtons();
//					
//					var scoreOffset:uint = _data.points;
//					scoreOffset *= -1;
//					dispatchEvent(new GameEvent(GameEvent.UPDATE_SCORE, scoreOffset));
//					
//					clearTimeout(_backToCategoryID);
//					_backToCategoryID = setTimeout(backToCategory, _dataModel.backToCategoryDelay * 1000);
//				}
//				hideCounter();
//			}
		}

		private function stopCounter():void
		{
			_counter.circle.stop();
		}
		
		private function resumeCounter():void
		{
			trace("INFO GameView :: resumeCounter");
			
			if (_isCounterPaused) {
				_isCounterPaused = false;
				var nextFrame:uint = 6 - _secLeft;
				_counter.gotoAndStop(nextFrame);
				_counter.circle.play();
				_updateCounterID = setInterval(updateCounter, 1000);
			}
		}
		
		private function deactivateButtons():void
		{
			var currentLength:uint = _buttonArr.length;
			for (var i:uint=0; i<currentLength; i++) {
				var thisButton:AnswerButton = _buttonArr[i];
				AnswerButton(thisButton).deactivate();
				thisButton.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				thisButton.removeEventListener(MouseEvent.MOUSE_UP, onAnswerSelected);
				thisButton.removeEventListener(MouseEvent.MOUSE_OUT, onAnswerSelected);
			}
		}
		
		private function showCorrectAnswer():void
		{
			var correctAnswerButton:AnswerButton = _buttonArr[_answerIndex];
			AnswerButton(correctAnswerButton).selected = true;
		}
		
		private function updateScoreOffset(pScoreOffset:uint):void
		{
			dispatchEvent(new GameEvent(GameEvent.UPDATE_SCORE, pScoreOffset));
		}
		
		private function playCorrect(pVolume:Number = .3):void
		{
			_soundController.playCorrect(pVolume);
		}
		
		private function playInCorrect(pVolume:Number = .7):void
		{
			_soundController.playInCorrect(pVolume);
		}
		
		private function backToCategory():void
		{
			trace("INFO GameView :: backToCategory");
			
			dispatchEvent(new Event("backToCategory"));
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public override function destroy():void
		{
			killTimer();
			
			if (_buttonArr.length > 0) {
				var currentLength:uint = _buttonArr.length;
				for (var i:uint=0; i<currentLength; i++) {
					var thisButton:AnswerButton = _buttonArr[i];
					thisButton.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
					thisButton.removeEventListener(MouseEvent.MOUSE_UP, onAnswerSelected);
					_buttonHolder.removeChild(_buttonArr[i]);
					_buttonArr[i] = null;
				}
				_buttonArr = [];
			}
			
			if (_buttonHolder) {
				_content.removeChild(_buttonHolder);
				_buttonHolder = null;
			}
			
			if (_content) {
				removeChild(_content);
				_content = null;
			}
		}

		public function killTimer():void
		{
			trace("INFO GameView :: killTimer");
			
			clearInterval(_updateCounterID);
			clearInterval(_checkSecPassedID);
			clearTimeout(_backToCategoryID);
			stopCounter();
		}
		
		public function stopTimer():void
		{
			trace("INFO GameView :: stopTimer");
			
			clearInterval(_checkSecPassedID);
			clearInterval(_updateCounterID);
			clearTimeout(_backToCategoryID);
			
			_isTimerStopped = true;
			if (_counter.visible) {
				_isCounterPaused = true;
				stopCounter();
			}
		}
		
		public function restartTimer():void
		{
			trace("INFO GameView :: restartTimer");
			
			if (_isTimerStopped) {
				_isTimerStopped = false;
				
				checkSecPassed();
				_checkSecPassedID = setInterval(checkSecPassed, 1000);
				
				if (_counter.visible) resumeCounter();
				
				if (_answerSelected) {
					_backToCategoryID = setTimeout(backToCategory, 1000);
				}
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onButtonPressed(e:MouseEvent):void
		{
//			trace("INFO GameView :: onButtonPressed, e.target is " + e.target);
			
			_soundController.playClick();
		}
		
		private function onAnswerSelected(e:MouseEvent):void
		{
//			trace("INFO GameView :: onAnswerSelected, index is " + e.target.index);

			clearInterval(_updateCounterID);
			clearInterval(_checkSecPassedID);
			
			_answerSelected = true;
			
			var selectedButton:AnswerButton = AnswerButton(e.target);
			var buttonIndex:uint = selectedButton.index;
			var answerID:String = selectedButton.answerID;
			selectedButton.selected = true;
			
			_dataModel.addSelectedAnswer(answerID);
			
			var scoreOffset:uint = _data.points;
			if (buttonIndex != _answerIndex) {
				TweenLite.delayedCall(.5, playInCorrect);
				TweenLite.delayedCall(.5, showCorrectAnswer);
				scoreOffset *= -1;
				TweenLite.delayedCall(.5, updateScoreOffset, [scoreOffset]);
			} else {
				TweenLite.delayedCall(.4, playCorrect);
				updateScoreOffset(scoreOffset);
			}
			deactivateButtons();
			hideCounter();
			
			_backToCategoryID = setTimeout(backToCategory, _dataModel.backToCategoryDelay * 1000);
			
//			dispatchEvent(new Event("deactivateButtons"));
		}
	}//c
}//p