package com.ges.view {
	import com.ges.assets.ISIPanel;
	import com.ges.assets.RefPanel;
	import com.ges.assets.QuitPanel;
	import com.ges.events.OverlayEvent;
	import com.ges.manager.*;
	import com.ges.ui.ISIPane;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;
	import com.greensock.easing.Quint;
	import com.greensock.easing.Expo;
	
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="close", type="com.ges.events.OverlayEvent")]
	[Event(name="continue", type="com.ges.events.OverlayEvent")]
	[Event(name="quit", type="com.ges.events.OverlayEvent")]
	public class OverlayPanelView extends BaseView {

		public static const ISI					:String = "isi";
		public static const REF					:String = "ref";
		public static const QUIT				:String = "quit";
		
		private var _kind						:String = ISI;
		private var _content					:MovieClip;
		
		private var _bkg						:MovieClip;
		private var _popup						:MovieClip;
		private var _closeBt					:MovieClip;
		private var _continueBt					:MovieClip;
		private var _quitBt						:MovieClip;
		
		private var _isiPane					:ISIPane;
		
		private var _refID						:uint = 11;
		
		
		public function OverlayPanelView(pPanelKind:String = "isi", pRefID:uint = 11) {
			super();
			viewName = "OverlayPanel";
			
			_kind = pPanelKind.toLowerCase();
			_refID = pRefID;
		}
		
		override protected function configUI() : void
		{
			_controller.registerView(Controller.OVERLAY_VIEW, this);
			
			var _isCloseBt:Boolean = true;
			switch (_kind.toLowerCase()) {
				case REF:
					_content = new RefPanel();
					break;
				case QUIT:
					_content = new QuitPanel();
					_isCloseBt = false;
					break;
				case ISI:
				default:
					_content = new ISIPanel();
			}
			addChild(_content);
			
			_bkg = _content.bkg;
			_bkg.mouseEnabled = false;
			_popup = _content.popup;
			
			if (_kind == REF) _popup.gotoAndStop("ref" + _refID);
			
			if (_isCloseBt) {
				_closeBt = _content.popup.closeBt;
				_closeBt.buttonMode = _closeBt.mouseEnabled = true;
				_closeBt.mouseChildren = false;
				
				_closeBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				_closeBt.addEventListener(MouseEvent.MOUSE_OUT, onCloseRequested);
				_closeBt.addEventListener(MouseEvent.MOUSE_UP, onCloseRequested);
			} else {
				_continueBt = _content.popup.continueBt;
				_quitBt = _content.popup.quitBt;
				_continueBt.buttonMode = _continueBt.mouseEnabled = true;
				_quitBt.buttonMode = _quitBt.mouseEnabled = true;
				_continueBt.mouseChildren = _quitBt.mouseChildren = false;
				
				_continueBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				_continueBt.addEventListener(MouseEvent.MOUSE_OUT, onContinueRequested);
				_continueBt.addEventListener(MouseEvent.MOUSE_UP, onContinueRequested);
				
				_quitBt.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				_quitBt.addEventListener(MouseEvent.MOUSE_OUT, onQuitRequested);
				_quitBt.addEventListener(MouseEvent.MOUSE_UP, onQuitRequested);
			}
			
			_popup.width = _popup.height = 1;
			_popup.visible = false;
			
			if (_kind == "isi") {
				buildISIPane();
			}
		}
		
		override public function destroy():void 
		{
			if (_continueBt) {
				_continueBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				_continueBt.removeEventListener(MouseEvent.MOUSE_OUT, onContinueRequested);
				_continueBt.removeEventListener(MouseEvent.MOUSE_UP, onContinueRequested);
				_continueBt = null;
			}
			
			if (_quitBt) {
				_quitBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				_quitBt.removeEventListener(MouseEvent.MOUSE_OUT, onQuitRequested);
				_quitBt.removeEventListener(MouseEvent.MOUSE_UP, onQuitRequested);
				_quitBt = null;
			}
			
			if (_closeBt) {
//				_closeBt.removeEventListener(MouseEvent.CLICK, onCloseRequested);
				_closeBt.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				_closeBt.removeEventListener(MouseEvent.MOUSE_OUT, onCloseRequested);
				_closeBt.removeEventListener(MouseEvent.MOUSE_UP, onCloseRequested);
				_closeBt = null;
			}
			
			if (_isiPane) {
				_popup.removeChild(_isiPane);
				_isiPane = null;
			}
			if (_content) {
				removeChild(_content);
				_content = null;
			}
		}
		
		override public function show():void 
		{
			_popup.width = _popup.height = 1;
			_popup.visible = true;
			TweenLite.to(_popup, .3, {scaleX:1, scaleY:1, ease:Quint.easeOut});
		}
		
		private function buildISIPane():void
		{
//			trace("INFO OverlayPanelView :: buildISIPane");
			_isiPane = new ISIPane();
			_popup.addChild(_isiPane);
		}
		
		private function closePopup():void
		{
			TweenLite.to(_popup, .3, {width:1, height:1, alpha:0, ease:Expo.easeOut, onComplete:function() {_popup.visible = false;} });
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onButtonPressed(e:MouseEvent):void
		{
			trace("INFO OverlayPanelView :: onButtonPressed, e.target is " + e.target);
			
			_soundController.playClick();
			
			var target:MovieClip = e.target as MovieClip;
			target.isDown = true;
			TweenLite.killTweensOf(target);
			TweenLite.to(target, .1, {colorTransform:{exposure:.85}}); 
		}
		
//		private function onButtonOut(e:MouseEvent):void
//		{
//			trace("INFO OverlayPanelView :: onButtonOut, e.target is " + e.target);
//			
//			var target:MovieClip = e.target as MovieClip;
//			TweenLite.killTweensOf(target);
//			TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 
//		}
		
		private function onCloseRequested(e:MouseEvent):void
		{
//			_soundController.playClick();

			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}});
				
				closePopup();
				TweenLite.to(_bkg, .3, {alpha:0, ease:Quad.easeOut, onComplete: function() {dispatchEvent(new OverlayEvent(OverlayEvent.CLOSE));}});
			}
		}
		
		private function onContinueRequested(e:MouseEvent):void
		{
			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}});
			
				closePopup();
				TweenLite.to(_bkg, .3, {alpha:0, ease:Quad.easeOut, onComplete: function() {dispatchEvent(new OverlayEvent(OverlayEvent.CONTINUE));}});
			}
		}
		
		private function onQuitRequested(e:MouseEvent):void
		{
			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 
			
				closePopup();
				TweenLite.to(_bkg, .3, {alpha:0, ease:Quad.easeOut, onComplete: function() {dispatchEvent(new OverlayEvent(OverlayEvent.QUIT));}});
			}
		}
	}//c
}//p