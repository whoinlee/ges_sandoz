package com.ges.view {
	import com.base.utils.TextUtils;
	
	import com.ges.assets.CategoryContent;
	import com.ges.events.SelectEvent;
	import com.ges.manager.*;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import com.greensock.TweenLite;
	
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="select", type="com.ges.events.SelectEvent")]
	public class CategoryView extends BaseView {
		
		public static const BUTTON_TOTAL		:uint = 12;
		private var _content					:CategoryContent;
		
		private var _res1Bt						:MovieClip;
		private var _res2Bt						:MovieClip;
		private var _res3Bt						:MovieClip;
		private var _res4Bt						:MovieClip;
		
		private var _stu1Bt						:MovieClip;
		private var _stu2Bt						:MovieClip;
		private var _stu3Bt						:MovieClip;
		private var _stu4Bt						:MovieClip;
		
		private var _saf1Bt						:MovieClip;
		private var _saf2Bt						:MovieClip;
		private var _saf3Bt						:MovieClip;
		private var _saf4Bt						:MovieClip;
		
		private var _selectedButtonCount		:uint = 0;
		public function get selectedButtonCount():uint
		{
			return _selectedButtonCount;
		}
		
		
		public function CategoryView() 
		{
			super();
			viewName = "Category";
		}
		
		override protected function configUI() : void
		{
//			trace("INFO CategoryView :: configUI");
//			TextUtils.traceFonts();

			_controller.registerView(Controller.CATEGORY_VIEW, this);
			
			_content = new CategoryContent();
			addChild(_content);
			
			for (var i=1; i<=4; i++) {
				this["_res" + i + "Bt"] = _content["res" + i + "Bt"];
				this["_stu" + i + "Bt"] = _content["stu" + i + "Bt"];
				this["_saf" + i + "Bt"] = _content["saf" + i + "Bt"];
				
				this["_res" + i + "Bt"].buttonMode = true;
				this["_stu" + i + "Bt"].buttonMode = true;
				this["_saf" + i + "Bt"].buttonMode = true;
				this["_res" + i + "Bt"].mouseEnabled = true;
				this["_stu" + i + "Bt"].mouseEnabled = true;
				this["_saf" + i + "Bt"].mouseEnabled = true;
				this["_res" + i + "Bt"].mouseChildren = false;
				this["_stu" + i + "Bt"].mouseChildren = false;
				this["_saf" + i + "Bt"].mouseChildren = false;
				
				this["_res" + i + "Bt"].selectedIndex = 1*10 + i;
				this["_stu" + i + "Bt"].selectedIndex = 2*10 + i;
				this["_saf" + i + "Bt"].selectedIndex = 3*10 + i;
				
				this["_res" + i + "Bt"].addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				this["_res" + i + "Bt"].addEventListener(MouseEvent.MOUSE_UP, onCategorySelected);
				this["_res" + i + "Bt"].addEventListener(MouseEvent.MOUSE_OUT, onCategorySelected);
				
				this["_stu" + i + "Bt"].addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				this["_stu" + i + "Bt"].addEventListener(MouseEvent.MOUSE_UP, onCategorySelected);
				this["_stu" + i + "Bt"].addEventListener(MouseEvent.MOUSE_OUT, onCategorySelected);
				
				this["_saf" + i + "Bt"].addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				this["_saf" + i + "Bt"].addEventListener(MouseEvent.MOUSE_UP, onCategorySelected);
				this["_saf" + i + "Bt"].addEventListener(MouseEvent.MOUSE_OUT, onCategorySelected);
			}
		}
		
		public override function destroy():void
		{
			for (var i=1; i<=4; i++) {
				this["_res" + i + "Bt"].removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				this["_stu" + i + "Bt"].removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				this["_saf" + i + "Bt"].removeEventListener(MouseEvent.MOUSE_DOWN, onButtonPressed);
				
				this["_res" + i + "Bt"].removeEventListener(MouseEvent.MOUSE_UP, onCategorySelected);
				this["_stu" + i + "Bt"].removeEventListener(MouseEvent.MOUSE_UP, onCategorySelected);
				this["_saf" + i + "Bt"].removeEventListener(MouseEvent.MOUSE_UP, onCategorySelected);
				
				this["_res" + i + "Bt"].removeEventListener(MouseEvent.MOUSE_OUT, onCategorySelected);
				this["_stu" + i + "Bt"].removeEventListener(MouseEvent.MOUSE_OUT, onCategorySelected);
				this["_saf" + i + "Bt"].removeEventListener(MouseEvent.MOUSE_OUT, onCategorySelected);
			}
			
			if (_content) {
				removeChild(_content);
				_content = null;
			}
		}

		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onButtonPressed(e:MouseEvent):void
		{
//			trace("INFO CategoryView :: onButtonPressed");
			
			_soundController.playClick();
			
			var target:MovieClip = e.target as MovieClip;
			TweenLite.killTweensOf(target);
			TweenLite.to(target, .1, {colorTransform:{exposure:.85}});
			target.isDown = true;
		}
		
		private function onCategorySelected(e:MouseEvent):void
		{
			var target:MovieClip = e.target as MovieClip;
			if (target.isDown) {
				_selectedButtonCount++;
				
				target.isDown = false;
				TweenLite.killTweensOf(target);
				TweenLite.to(target, .1, {colorTransform:{exposure:1}}); 

				target.removeEventListener(MouseEvent.MOUSE_UP, onCategorySelected);
				target.buttonMode = target.mouseEnabled = false;
				target.alpha = .5;
	
				var selectedIndex:uint = target.selectedIndex;
				dispatchEvent(new SelectEvent(SelectEvent.SELECT, selectedIndex));
			}
		}
	}//c
}//p