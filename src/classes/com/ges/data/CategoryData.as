package com.ges.data {

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class CategoryData  {
		
		private var _name						:String = "RESULTS";
		private var _quizDataArr				:Array;	
		private var _textX						:Number = 400;
		
		public function get name():String
		{
			return _name;
		}
		public function get quizDataArr():Array
		{
			return _quizDataArr;
		}
		public function get textX():Number
		{
			return _textX;
		}
		
		
		public function CategoryData(categoryItem:XML)
		{
			_name = categoryItem.@name.toUpperCase();
			_textX = Number(categoryItem.@x);
			
			_quizDataArr = new Array();	
			var quizDataList:XMLList = categoryItem.quiz;
			for each (var quizDataItem:XML in quizDataList) {
				var quizData:QuizData = new QuizData(_name, _textX, quizDataItem);
				_quizDataArr.push(quizData);
			}
		}
	}//c
}//p