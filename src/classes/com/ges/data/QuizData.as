package com.ges.data {

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class QuizData  {
		
		private var _category					:String = "TOTALITY OF EVIDENCE";
		private var _categoryX					:Number = 400;
		
		private var _points						:uint = 200;
		private var _id							:String = "";
		
		private var _question					:String = "";
		private var _questionY					:Number = 260;
		private var _answerY					:Number = 400;
		
		private var _answerDataArr				:Array = [];
		private var _answerIndex				:uint = 0;
		private var _trademarkArr				:Array = [];
		private var _refTypeID					:uint = 11;
		
		private var _isSpecial					:Boolean = false;
		

		public function get category():String
		{
			return _category;
		}
		public function get categoryX():Number
		{
			return _categoryX;
		}
		
		public function get points():uint
		{
			return _points;
		}
		public function get id():String
		{
			return _id;
		}
		
		public function get question():String
		{
			return _question;
		}
		public function get questionY():Number
		{
			return _questionY;
		}
		public function get answerY():Number
		{
			return _answerY;
		}

		public function get answerDataArr():Array
		{
			return _answerDataArr;
		}
		public function get answerIndex():uint
		{
			return _answerIndex;
		}
		public function get trademarkArr():Array
		{
			return _trademarkArr;
		}
		public function get refTypeID():uint
		{
			return _refTypeID;
		}
		
		public function get isSpecial():Boolean
		{
			return _isSpecial;
		}

		
		public function QuizData(pCategory:String, pCategoryX:Number, quizDataItem:XML)
		{
			_category = pCategory;
			_categoryX = pCategoryX;
			
			_points = Number(quizDataItem.@points);
			_id = quizDataItem.@id;
			_question = quizDataItem.question.toString();
			_questionY = Number(quizDataItem.question.@y);
			_answerY = Number(quizDataItem.answers.@y);
			_isSpecial = (quizDataItem.answers.@isSpecial.toLowerCase() == "true")? true: false;
			if (quizDataItem.answers.@trademarks != "") {
				_trademarkArr = quizDataItem.answers.@trademarks.split("/");
			}
			_refTypeID = Number(quizDataItem.answers.@refType);
			
			_answerDataArr = new Array();	
			var answerDataList:XMLList = quizDataItem.answers.answer;
			var index:uint = 0;
//			trace("INFO QuizData answerDataList.length is " + answerDataList.length + ", _question:" + _question);
			for each (var answerDataItem:XML in answerDataList) {
				var answerData:AnswerData = new AnswerData(index, answerDataItem, _isSpecial);
				_answerDataArr.push(answerData);
				if (answerData.isAnswer) _answerIndex = index;
				index++;
			}
		}
	}//c
}//p