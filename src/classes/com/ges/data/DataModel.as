package com.ges.data {
	import com.ges.Config;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="dataReady", type="flash.events.Event")]
	public class DataModel extends EventDispatcher
	{
		private static var instance				:DataModel;
		
		public static const DATA_READY			:String = "dataReady";
		
		private var _dataLoader					:URLLoader;
		private var _isDataReady				:Boolean = false;
		public function get isDataReady():Boolean
		{
			return _isDataReady;
		}
		
		private var _top10Arr					:Array = [];
		public function get top10Arr()			:Array
		{
			return _top10Arr;
		}
		
		private var _top3Arr					:Array = [];
		public function get top3Arr()			:Array
		{
			_top3Arr = [];
			if (_top10Arr.length > 0) {
				if (_top10Arr.length >= 3) {
					_top3Arr = _top10Arr.slice(0,3);
				} else {
					_top3Arr = _top10Arr.slice(0);
				}
			}
			return _top3Arr;
		}
		
		private var _currentUser				:UserData;
		public function get currentUser()		:UserData
		{
			return _currentUser;
		}
		public function set currentUser(pUser:UserData):void
		{
			_currentUser = pUser;
		}
		
		private var _gameTimeout				:uint = 120;
		public function get gameTimeout()		:uint
		{
			return _gameTimeout;
		}
		
		private var _keyboardTimeout			:uint = 30;
		public function get keyboardTimeout()	:uint
		{
			return _keyboardTimeout;
		}
		
		private var _leaderboardTimeout			:uint = 10;
		public function get leaderboardTimeout():uint
		{
			return _leaderboardTimeout;
		}
		
		private var _backToCategoryDelay		:uint = 3;
		public function get backToCategoryDelay():uint
		{
			return _backToCategoryDelay;
		}
		
		private var _categoryDataArr			:Array;
		public function get categoryDataArr()	:Array
		{
			return _categoryDataArr;
		}
		
		private var _selectedAnswerArr			:Array = [];
		public function get selectedAnswerArr()	:Array
		{
			return _selectedAnswerArr;
		}
		
		
		///////////////////////
		//-- constructor
		///////////////////////
		public function DataModel(enforcer:SingletonEnforcer)
		{
			
		}
		
		public static function getInstance():DataModel 
		{
			if (DataModel.instance == null) {
				DataModel.instance = new DataModel(new SingletonEnforcer());
			}
			return DataModel.instance;
		}
		
		
		///////////////////////
		//-- public methods
		///////////////////////
		public function getData(path:String):void
		{	
//			trace("INFO DataModel :: getData");	
			
			_dataLoader = new URLLoader();

			var urlReq:URLRequest = new URLRequest(path);
			_dataLoader.addEventListener(Event.COMPLETE, onDataLoaded);
			_dataLoader.load(urlReq);
		}
		
		public function setData(xml:XML):void
		{
//			trace("INFO DataModel :: setData");	
			
			//--appVars
			Config.hideCursor = (xml.appVars.hideCursor.toString().toLowerCase() == "true")? true:false;
			Config.fullScreen = (xml.appVars.fullScreen.toString().toLowerCase() == "true")? true:false;
			Config.isSwipe = (xml.appVars.isSwipe.toString().toLowerCase() == "true")? true:false;	//-- udpated by Valerie
			Config.idleTimeout = Number(xml.appVars.idleTimeout) * 1000;
			Config.attractPath = xml.appVars.attractPath.toString();
			
			//--gameVars
			_gameTimeout = Number(xml.gameVars.gameTimeout);
			_keyboardTimeout = Number(xml.gameVars.keyboardTimeout);
			_leaderboardTimeout = Number(xml.gameVars.leaderboardTimeout);
			_backToCategoryDelay = Number(xml.gameVars.backToCategoryDelay);
			
			//--gameVars : categories
			var categoryList:XMLList = xml.gameVars.categories.category;
			_categoryDataArr = [];
			for each (var categoryItem:XML in categoryList) {
				var categoryData:CategoryData = new CategoryData(categoryItem);
				_categoryDataArr.push(categoryData);
			}
			
			//-- for testing
			var testingData:Array = [	{name:"Tester1", acro:"CMB", score:1100}, 
										{name:"Tester2", acro:"RSG", score:900}, 
										{name:"Tester3", acro:"JGT", score:800}];
			for (var i=0; i<3; i++) {
				var newUser:UserData = new UserData(testingData[i].name);
				newUser.acro = testingData[i].acro;
				newUser.score = testingData[i].score;
				_top10Arr.push(newUser);
			}
		}
		
		public function reset():void
		{
			trace("INFO DataModel :: reset");
			_selectedAnswerArr = [];
		}
		
		public function isTop10(pUser:UserData = null):Boolean
		{
			if (pUser == null) pUser = _currentUser;
			
			var currentScore:int = pUser.score;
			if (currentScore < 0) return false;
			
//			trace("INFO DataModel :: isTop10, ever??, currentScore is ---------->" + currentScore);
			var isAdding:Boolean = false;
			var total:uint = _top10Arr.length;
			
			if (total < 10) {
				isAdding = true;
			} else if (_top10Arr[9].score < currentScore) {
				isAdding = true;
			}
			return isAdding;
		}
		
		public function addUser(pUser:UserData = null):Boolean
		{
//			trace("INFO DataModel :: addUser, _top10Arr.length is " + _top10Arr.length);
			
			if (pUser == null) pUser = _currentUser;
			var currentScore:int = pUser.score;
			if (currentScore <0) return false;
			
//			trace("INFO DataModel :: addUser, ever??, currentScore is ---------->" + currentScore);
			var ranking:uint = 0;
			var total:uint = _top10Arr.length;
			var isAdding:Boolean = false;
			if (total < 10) {
				isAdding = true;
			} else if (_top10Arr[9].score < currentScore) {
				_top10Arr.pop();
				isAdding = true;
				total--;
			} //-- if not, can't add the current user to top 10;
			
			if (isAdding) {
				var targetIndex:uint = total;	//the last index + 1
				for (var i=0; i<total; i++) {
					var userData:UserData = _top10Arr[i];
					var userScore:int = userData.score;
					if (userScore < currentScore) {
						targetIndex = i;
						break;
					}
				}
				if (targetIndex < total) {
					_top10Arr.splice(targetIndex, 0, pUser);
				} else {
					_top10Arr.push(pUser);
				}
				ranking = targetIndex + 1;
			}
			pUser.ranking = ranking;

			if (_top10Arr.length >= 3) {
				_top3Arr = _top10Arr.slice(0,3);
			} else {
				_top3Arr = _top10Arr.slice(0);
			}
			
			return isAdding;
		}
		
		public function addSelectedAnswer(answerID:String):void
		{
			_selectedAnswerArr.push(answerID);
		}
		

		///////////////////////
		//-- event handlers
		///////////////////////
		private function onDataLoaded(e:Event):void
		{
//			trace("INFO DataModel, onDataLoaded");
			
			_dataLoader.removeEventListener(Event.COMPLETE, onDataLoaded);
			
			var xml:XML = XML(_dataLoader.data);
			setData(xml);
			xml = null;
			_isDataReady = true;
			dispatchEvent(new Event(DATA_READY));
		}
	}//c
}//p

class SingletonEnforcer {}