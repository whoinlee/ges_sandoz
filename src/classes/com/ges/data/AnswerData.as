package com.ges.data {

	/**
	 * @author WhoIN Lee: whoin@hotmail.com
	 */
	public class AnswerData  {
		
		private var _index						:uint = 0;
		private var _isAnswer					:Boolean = false;
		private var _answer						:String = "";
		private var _isSpecial					:Boolean = false;
		private var _id							:String = "";
		
		
		public function get index():uint
		{
			return _index;
		}

		public function get isAnswer():Boolean
		{
			return _isAnswer;
		}
		
		public function get answer():String
		{
			return _answer;
		}

		public function get isSpecial():Boolean
		{
			return _isSpecial;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		
		public function AnswerData(pIndex:uint, answerDataItem:XML, pIsSpecial:Boolean = false)
		{
			_index = pIndex;
			_isSpecial = pIsSpecial;
			
			_isAnswer = (answerDataItem.@isAnswer.toLowerCase() == "true")? true: false;
			_id = answerDataItem.@id;
			_answer = answerDataItem.toString();
		}
	}//c
}//p