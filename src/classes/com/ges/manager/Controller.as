package com.ges.manager {
	import com.ges.data.DataModel;
	import com.ges.events.*;
	import com.ges.view.*;

	import flash.display.Sprite;
	import flash.events.*;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class Controller extends EventDispatcher
	{
		private static var instance				:Controller;
		
		//-- view name list
		public static const APP					:String = "Main";
		public static const WELCOME_VIEW		:String = "Welcome";
		public static const CATEGORY_VIEW		:String = "Category";
		public static const OVERLAY_VIEW		:String = "OverlayPanel";
		public static const SCOREPANEL1_VIEW	:String = "ScorePanel1";
		public static const SCOREPANEL2_VIEW	:String = "ScorePanel2";
		public static const GAME_VIEW			:String = "Game";
		public static const RESULT_VIEW			:String = "Result";
		
		private var _app						:*; 			//-- reference to the application instance
		private var _views						:Object; 		//-- holds named list of view instances
		private var _dataModel					:DataModel;
		
		private var _scoreView					:ScorePanel2View;
		
		
		public function Controller(enforcer:SingletonEnforcer) 
		{
//			trace("INFO Controller :: dummy param enforcer is " + enforcer);
			_views = new Object();
			_dataModel = DataModel.getInstance();
		}
		
		public static function getInstance():Controller
		{
			if (instance == null) {
				instance = new Controller(new SingletonEnforcer());
			}
			return instance;	
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public function registerApp ( app:* ):void
		{
			_app = app;
		}
		
		public function registerView ( viewName:String, view:Sprite ):void
		{
			//trace ("INFO Controller :: registerView - viewName is " + viewName + ", view is " + view);
			_views [ viewName ] = view;
			switch (viewName) {
				case WELCOME_VIEW:
					view.addEventListener(GameEvent.START, onStartGameRequested);
					view.addEventListener(GameEvent.ISI, onShowISI);
					view.addEventListener(GameEvent.REF, onShowRef);
					view.addEventListener(GameEvent.QUIT, onQuitGame1Requested);
					break;
				case CATEGORY_VIEW:
					view.addEventListener(SelectEvent.SELECT, onCategorySelected);
					break;
				case SCOREPANEL2_VIEW:
					_scoreView = view as ScorePanel2View;
					view.addEventListener(GameEvent.ISI, onShowISI);
					view.addEventListener(GameEvent.REF, onShowRef);
					view.addEventListener(GameEvent.QUIT, onQuitGame1Requested);
					view.addEventListener(GameEvent.TIMEOUT, onTimeOut);
					break;
				case OVERLAY_VIEW:
					view.addEventListener(OverlayEvent.CLOSE, onCloseOverlayRequested);
					view.addEventListener(OverlayEvent.CONTINUE, onContinueGameRequested);
					view.addEventListener(OverlayEvent.QUIT, onQuitGame2Requested);
					break;
				case GAME_VIEW:
					view.addEventListener(GameEvent.UPDATE_SCORE, onScoreUpdateRequested);
					view.addEventListener("backToCategory", onBackToCategoryRequested);
					break;
				case RESULT_VIEW:
					view.addEventListener(GameEvent.RESTART, onRestartGameRequested);
					view.addEventListener(GameEvent.ISI, onShowISI);
					view.addEventListener(GameEvent.QUIT, onQuitGame3Requested);
					break;
			}
		}
	
		public function getView ( viewName:String ) : Object
		{
			return (_views [ viewName ]);
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onStartGameRequested(e:GameEvent):void
		{
			_app.startGame();
		}
		
		private function onRestartGameRequested(e:GameEvent):void
		{
			_app.restartGame();
		}

		private function onQuitGame1Requested(e:GameEvent):void
		{
//			trace("INFO Controller :: onQuitGame1Requested, from start page or score panel2");
			//-- the quit button on the bottom pressed
			_app.buildOverlay(OverlayPanelView.QUIT);
		}
		
		private function onQuitGame2Requested(e:OverlayEvent):void
		{
//			trace("INFO Controller :: onQuitGame2Requested, from overlay panel");

			//-- when the quit button on the overlay pressed
//			_scoreView.killTimer();
			_app.destroyOverlay();
			_app.quitGame();
		}
		
		private function onQuitGame3Requested(e:GameEvent):void
		{
//			trace("INFO Controller :: onQuitGame3Requested, from result page");
			//-- when the quit button on the resultPage pressed
			_app.quitGame();
		}
		
		private function onShowISI(e:GameEvent):void
		{
			_app.buildOverlay(OverlayPanelView.ISI);
//			if (_scoreView) _scoreView.stopTimer();	//moved to Main 5/20
		}
		
		private function onShowRef(e:GameEvent):void
		{
			_app.buildOverlay(OverlayPanelView.REF);
//			if (_scoreView) _scoreView.stopTimer();//moved to Main 5/20
		}
		
		private function onCloseOverlayRequested(e:OverlayEvent):void
		{
			_app.destroyOverlay();
//			if (_scoreView) _scoreView.restartTimer();	//moved to Main 5/20
		}
		
		private function onContinueGameRequested(e:OverlayEvent):void
		{
//			trace("INFO Controller :: onContinueGameRequested");	
			_app.destroyOverlay();
		}
		
		private function onCategorySelected(e:SelectEvent):void
		{
//			trace("INFO Controller :: onCategorySelected, e.selectedIndex is " + e.selectedIndex);		
			var categoryIndex:uint = Math.floor(e.selectedIndex/10 - 1);
			var questionIndex:uint = e.selectedIndex%10 - 1;
			_app.showQuiz(categoryIndex, questionIndex);
		}
		
		private function onScoreUpdateRequested(e:GameEvent):void
		{
//			trace("\nINFO Controller :: onScoreUpdateRequested");		
			var scoreOffset:int = e.scoreOffset;
			var scoreView:ScorePanel2View = ScorePanel2View(_views[SCOREPANEL2_VIEW]);
			scoreView.updateScore(scoreOffset);
			
//			var delay:uint = _dataModel.backToCategoryDelay;	//in seconds
//			_app.backToCategory(delay);
		}
		
		private function onTimeOut(e:GameEvent):void
		{
//			trace("INFO Controller :: onTimeOut");
			_app.onTimeOut();
		}
		
		private function onBackToCategoryRequested(e:Event):void
		{
			trace("INFO Controller :: onBackToCategoryRequested");
//			var scoreView:ScorePanel2View = ScorePanel2View(_views[SCOREPANEL2_VIEW]);
//			scoreView.deactivateButtons();
			_app.backToCategory();
		}
	}//c
}//p

class SingletonEnforcer {}