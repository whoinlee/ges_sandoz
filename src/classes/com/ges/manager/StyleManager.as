package com.ges.manager {
	import com.base.utils.TextUtils;

	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.StyleSheet;
	import flash.text.TextFormat;

	/*******************************************************************************
	* @author: WhoIN Lee
	* whoin@hotmail.com
	*******************************************************************************/
	public class StyleManager extends EventDispatcher {
		
		private static var instance					:StyleManager;
		
		public static const CSS_READY				:String = "cssReady";

		private static var _styleLoader				:URLLoader;
		
		private var __cssReady						:Boolean = false;
		public function get cssReady()				:Boolean	{ return (__cssReady); }
		
		private var __styleSheet					:StyleSheet;
		public function get styleSheet()			:StyleSheet	{ return (__styleSheet); }
		

		///////////////////////
		//-- constructor
		///////////////////////
		public function StyleManager(enforcer:SingletonEnforcer) 
		{
			trace("INFO StyleManager:: dummy " + enforcer);
			
			//initSuperscript
//			TextUtils.initSuperscript();	//no need. added to the assets.fla

//			TextUtils.traceFonts();
		}
		
		public static function getInstance():StyleManager
		{
			if (instance == null) {
				instance = new StyleManager(new SingletonEnforcer());
			}
			return instance;	
		}
		
		
		///////////////////////
		//-- public methods
		///////////////////////
		public function loadStyle(path:String):void
		{
			if (!__cssReady) {
//				trace("INFO StyleManager :: loadStyle, path is " + path);
				
				__styleSheet = new StyleSheet();
				_styleLoader = new URLLoader();
				var req:URLRequest = new URLRequest(path);
				_styleLoader.load(req);
				_styleLoader.addEventListener(Event.COMPLETE, styleLoadedHandler);
			}
		}

		public static function getQuestionFormat():TextFormat 
		{
			return TextUtils.parseCSS("p {font-family:AauxPro OT Bold; font-size:38px; color:#003463; text-align:center; leading:2pt;}");
		}


		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function styleLoadedHandler(event:Event):void 
		{
//			trace("\nINFO StyleManager :: styleLoadedHandler");
//			trace("\nINFO StyleManager :: styleLoadedHandler, _styleLoader.data is \n" + _styleLoader.data);
			
        	__styleSheet.parseCSS(_styleLoader.data);
			__cssReady = true;	
           	dispatchEvent(new Event(CSS_READY)); 
        }
		
	}//c
}//p

class SingletonEnforcer {}