package com.ges {

	public class Config  {

		public static const STAGE_W			:Number = 1920;
		public static const STAGE_H			:Number = 1080;
		public static const CONFIG_PATH		:String = "config/config.xml";
		public static const STYLE_PATH		:String = "config/style.css";
		
		public static var hideCursor 		:Boolean = false;
		public static var fullScreen 		:Boolean = true;
		public static var isSwipe 			:Boolean = false;	//-- udpated by Valerie
		public static var attractPath 		:String = "";
		public static var idleTimeout 		:Number = 0;
		
	}//c
}//p